# Neuro-Evolutionary Deep Reinforcement Learning

## `__data__`

This folder contains the configurations and save data for the various implemented agents.
When agents are run with no cold start, then the previously saved model will be loaded from this folder.

## Outline

### Folders

- [`.config`]( ./.config/): Holds the agents' configuration data per environment.
