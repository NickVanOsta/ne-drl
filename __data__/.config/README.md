# Neuro-Evolutionary Deep Reinforcement Learning

## `__data__/.config`

This folder contains the configurations the various implemented agents per environment.
There are configurations for several environments, currently including:

- CartPole-v1
- CliffWalking-v0
- FrozenLake-v1
- MountainCar-v1

When implementing for a new environment, make sure to add a new configuration file here for that particular environment.
