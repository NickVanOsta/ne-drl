# Release

[//]: # 'Add the release version to the Title above'

## Checklist

[//]: # 'This is a list of things to be checked by the reviewers'
[//]: # 'Please leave everything unchecked'

- [ ] Documentation has been added/updated where necessary.
- [ ] All texts and comments are in English.
- [ ] Added files reference license and initial author.
- [ ] Merging into `production`.
- [ ] MR is tagged correctly.
- [ ] Version has been bumped.
