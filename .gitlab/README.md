# Neuro-Evolutionary Deep Reinforcement Learning

## `.gitlab`

This folder contains templates for GitLab issues and Merge Requests.
The files in this folder should not be changed unless absolutely necessary.
