# Neuro-Evolutionary Deep Reinforcement Learning

## `logs`

This folder contains the logs for the data used in the "experimental results" chapter of the thesis.
There are logs for 7 different agents.
When running the `main.py` file through `make run`,
then the logs for that run will be generated here in a folder named after the timestamp of the run's start time.
