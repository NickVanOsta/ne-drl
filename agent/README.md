# Neuro-Evolutionary Deep Reinforcement Learning

## `agent`

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

This folder contains the Python 3 code for the `agent` module and its submodules.
This module handles everything to do with the implemented Reinforcement Learning Agents.

## Citing

When citing anything from the source code, please cite it as follows:

```bibtex
@mastersthesis{vanosta:2022,
  author = {Van Osta, Nick},
  title  = {Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control},
  school = {University Ghent},
  year   = {2022}
}
```

## Outline

### Folders

- [`nedrl`](./nedrl/): Contains the `nedrl` submodule, which handles the implementation of the NE-DRL framework.
- [`neuroevolution`](./neuroevolution/): Contains the `neuroevolution` submodule, which handles the implementation of NE agents such as the NEAT agent.
- [`q_learning`](./q_learning/): Contains the `q_learning` submodule, which handles the implementation of the Q-Learning agents, both tabular and deep.

### Files

- [`__init__.py`](./__init__.py): Contains the code for the `agent` module, mostly defines basic types and classes.
- [`action_select.py`](./action_select.py): Contains various action selection function implementations.
- [`loss.py`](./loss.py): Contains various loss function implementations.
- [`random.py`](./random.py): Contains the implementation for the random agent.
