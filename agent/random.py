"""
Author: Nick Van Osta
"""


from typing import List, Tuple

from gym import Env

from agent import Agent, EpisodeStopConditionOrLimit, RunStopConditionOrLimit
from agent.action_select import random_sample


class RandomAgent(Agent):
    """Represents an Agent that does not learn but randomly chooses actions."""

    def __init__(self, env: Env) -> None:
        super().__init__(env, 0, 0, 0, 1, 0, 0, 1)

    def fit(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
    ) -> Tuple[float, List[float], List[List[float]]]:
        return super().fit(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            random_sample(self._env),
        )

    def run(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
    ) -> Tuple[float, List[float], List[List[float]]]:
        return super().run(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            random_sample(self._env),
        )
