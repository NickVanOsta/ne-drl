"""
Author: Nick Van Osta
"""


import os
from typing import Any, Callable, Dict, List, Tuple, Union

import numpy as np
import tensorflow as tf
from gym import Env, spaces
from tqdm.auto import tqdm

from agent.loss import huber

SingleState = Union[int, float]
"""Represents a state instance within the environment that consists of a singular value."""
SingleAction = Union[int, float]
"""Represents a possible action within the environment that consists of a singular value."""
State = Union[SingleState, List[SingleState]]
"""Represents a state instance within the environment."""
Action = Union[SingleAction, List[SingleAction]]
"""Represents a possible action within the environment."""


class Percept(object):
    """Represents an Agent's observation within an environment."""

    def __init__(
        self,
        old_state: State,
        new_state: State,
        action: Action,
        reward: float,
        is_terminal: bool,
        info: Dict[str, Any],
    ) -> None:
        super().__init__()
        self.__old_state = old_state
        self.__new_state = new_state
        self.__action = action
        self.__reward = reward
        self.__is_terminal = is_terminal
        self.__info = info

        # self.__vector = np.array(
        #     [
        #         *Percept.__to_list(old_state),
        #         *Percept.__to_list(action),
        #         *Percept.__to_list(new_state),
        #     ]
        # )

    @property
    def s(self) -> State:
        """The original state the agent resided in."""
        return self.__old_state

    @property
    def s_new(self) -> State:
        """The new state the agent landed in."""
        return self.__new_state

    @property
    def a(self) -> Action:
        """The action the agent performed."""
        return self.__action

    @property
    def r(self) -> float:
        """The reward the agent obtained."""
        return self.__reward

    @r.setter
    def r(self, val: float) -> None:
        """The reward the agent obtained."""
        self.__reward = val

    @property
    def is_terminal(self) -> bool:
        """Whether the episode ended upon landing in the new state."""
        return self.__is_terminal

    @property
    def info(self) -> object:
        """Extra information about the environment."""
        return self.__info

    # # FAILED EXPERIMENT: Distance-Based Experience Replay
    # def distance(self, p: "Percept") -> float:
    #     return np.linalg.norm(self.__vector - p.__vector)

    # @staticmethod
    # def __is_singular(x: State):
    #     return not (
    #         isinstance(x, tuple)
    #         or isinstance(x, list)
    #         or isinstance(x, np.ndarray)
    #         or isinstance(x, tf.Tensor)
    #     )

    # @staticmethod
    # def __to_list(x: State) -> List[State]:
    #     return [x] if Percept.__is_singular(x) else x

    def __str__(self) -> str:
        return "[{}, {}, {}, {}, {}]".format(
            self.__old_state,
            self.__action,
            self.__new_state,
            self.__reward,
            self.__is_terminal,
        )

    def __repr__(self) -> str:
        return "[{}, {}, {}, {}, {}]".format(
            self.__old_state,
            self.__action,
            self.__new_state,
            self.__reward,
            self.__is_terminal,
        )


class ReplayBuffer:
    """Represents a general replay buffer used for memory replay."""

    def __init__(self, max_samples=4096, batch_size=64) -> None:
        self.max_samples = max_samples
        self.memory = np.empty(shape=(self.max_samples), dtype=np.ndarray)
        self.batch_size = batch_size
        self.n_entries = 0
        self.next_index = 0

    def update(self, idxs: np.ndarray, td_errors: np.ndarray):
        """Update the replay buffer."""
        pass

    def store(self, sample: Percept):
        """Store a new Percept into the replay buffer.

        Args:
            sample (Percept): The Percept sample to store.
        """

        self.memory[self.next_index] = sample

        self.next_index = (self.next_index + 1) % self.max_samples
        self.n_entries = min(self.n_entries + 1, self.max_samples)

    def sample(
        self, batch_size: int = None
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Gather a sample batch from the replay buffer.

        Args:
            batch_size (int, optional): The size of the batch sample.. Defaults to None.

        Returns:
            Tuple[np.ndarray, np.ndarray, np.ndarray]: List of indices, list of TD errors, and the batch samples.
        """

        if batch_size == None:
            batch_size = self.batch_size

        idxs = np.random.choice(self.n_entries, batch_size, replace=False)
        return idxs, np.ones_like(idxs), self.memory[idxs]

    def __len__(self):
        return self.n_entries

    def __repr__(self):
        return str(self.memory[: self.n_entries])

    def __str__(self):
        return str(self.memory[: self.n_entries])


class PrioritizedReplayBuffer(ReplayBuffer):
    """Represents a prioritized replay buffer used for prioritized experience replay (PER)."""

    __EPS = 0.01

    def __init__(
        self,
        max_samples=4096,
        batch_size=64,
        rank_based=False,
        alpha=0.6,
        beta0=0.1,
        beta_rate=0.99992,
    ) -> None:
        super().__init__(max_samples, batch_size)

        self.memory = np.empty(shape=(self.max_samples, 2), dtype=np.ndarray)
        self.td_error_index = 0
        self.sample_index = 1
        self.rank_based = rank_based
        self.alpha = alpha
        self.beta = beta0
        self.beta0 = beta0
        self.beta_rate = beta_rate

    def update(self, idxs, td_errors):
        self.memory[idxs, self.td_error_index] = np.abs(td_errors)

        if self.rank_based:
            sorted_arg = self.memory[: self.n_entries, self.td_error_index].argsort()[
                ::-1
            ]
            self.memory[: self.n_entries] = self.memory[sorted_arg]

    def _update_beta(self):
        """Update the beta parameter.

        Returns:
            float: The new value for the beta parameter.
        """

        self.beta = min(1.0, self.beta * self.beta_rate ** -1)
        return self.beta

    def store(self, sample: Percept):
        priority = 1.0
        if self.n_entries > 0:
            priority = self.memory[: self.n_entries, self.td_error_index].max()

        self.memory[self.next_index, self.td_error_index] = priority
        self.memory[self.next_index, self.sample_index] = sample

        self.n_entries = min(self.n_entries + 1, self.max_samples)
        self.next_index = (self.next_index + 1) % self.max_samples

    def sample(
        self, batch_size: int = None
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        if batch_size == None:
            batch_size = self.batch_size

        self._update_beta()
        entries = self.memory[: self.n_entries]

        if self.rank_based:
            priorities = 1 / (np.arange(self.n_entries) + 1)
        else:
            priorities = entries[:, self.td_error_index] + PrioritizedReplayBuffer.__EPS

        scaled_priorities = priorities ** self.alpha
        pri_sum = np.sum(scaled_priorities)
        probs = np.array(scaled_priorities / pri_sum, dtype=np.float64)

        weights = (self.n_entries * probs) ** -self.beta
        norm_weights = weights / weights.max()
        idxs = np.random.choice(self.n_entries, batch_size, replace=False, p=probs)
        samples = np.array([entries[idx] for idx in idxs])

        return idxs, norm_weights[idxs], samples[:, self.sample_index]


ActionSelect = Callable[[State], Action]
"""Represents an action selection method."""
LossFunction = Callable[[np.ndarray, np.ndarray], float]
"""Represents a loss function."""
ProcessPercept = Callable[[Percept], Percept]
"""Represents a percept processing function that returns the modified percept."""

EpisodeStopCondition = Callable[[int, List[Percept]], bool]
"""Represents a stop condition function for episodes that takes in the step index and returns whether to stop or not."""
EpisodeStopConditionOrLimit = Union[int, EpisodeStopCondition]
"""Represents a stop condition function for episodes or a step limit."""
RunStopCondition = Callable[[int, List[float]], bool]
"""Represents a stop condition function that takes in the step index and returns whether to stop or not."""
RunStopConditionOrLimit = Union[int, RunStopCondition]
"""Represents a stop condition function or a step limit."""

ModelBuilder = Callable[[State, Action], Any]
"""Represents a function to build a deep learning model."""
ModelPredictor = Callable[[Any, State], List[Action]]
"""Represents a function to predict a value from a deep learning model."""
ModelFitter = Callable[[Any, State, Action, LossFunction], Any]
"""Represents a function to fit a deep learning model."""
ModelCopier = Callable[[Any, Any], Any]
"""Represents a function to copy a deep learning model."""

ModelLoader = Callable[[str], Any]
"""Represents a function to load a deep learning model."""
ModelSaver = Callable[[Any, str], None]
"""Represents a function to save a deep learning model."""


class Agent(object):
    """Represents an Agent that can explore an environment's states by taking actions."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        super().__init__()
        self._env = env
        self._n_states = self._get_space_shape(env.observation_space)
        self._n_actions = self._get_space_shape(env.action_space)
        self._epsilon = epsilon_max
        self._epsilon_min = epsilon_min
        self._epsilon_max = epsilon_max
        self._epsilon_decay = epsilon_decay
        self._decay_frequency = decay_frequency
        self._learning_rate = learning_rate
        self._discount = discount
        self._save_file = save_file
        self._save_frequency = save_frequency
        self._process_percept = process_percept

        self._episode_number = 0
        self._step_number = 0
        self._total_steps = 0

        self._is_training: bool = False

        # Create directory structure for save file
        # or load if it already exists
        if self._save_file is not None:
            last_slash = self._save_file.rfind("/")
            if last_slash < 0:
                basedir = "."
            else:
                basedir = self._save_file[:last_slash]

            if not os.path.exists(basedir):
                os.makedirs(basedir)

            if cold_start:
                self._build_model()
            else:
                try:
                    self._load_model()
                except:
                    self._build_model()

    @property
    def env(self) -> Env:
        """The agent's assigned environment."""
        return self._env

    @property
    def env_is_discrete(self) -> bool:
        """Whether the environment has a discrete action space."""
        return "Discrete" in str(type(self._env.action_space))

    @property
    def n_states(self) -> int:
        """The number of states within the environment's state space."""
        return self._n_states

    @property
    def n_actions(self) -> int:
        """The number of actions within the environment's action space."""
        return self._n_actions

    @property
    def ε(self) -> float:
        """The current exploration rate."""
        return self._epsilon

    @ε.setter
    def ε(self, val: float) -> None:
        """The current exploration rate."""
        self._epsilon = val

    @property
    def ε_max(self) -> float:
        """The maximum exploration rate."""
        return self._epsilon_max

    @ε_max.setter
    def ε_max(self, val: float) -> None:
        """The maximum exploration rate."""
        self._epsilon_max = val

    @property
    def ε_min(self) -> float:
        """The minimum exploration rate."""
        return self._epsilon_min

    @ε_min.setter
    def ε_min(self, val: float) -> None:
        """The minimum exploration rate."""
        self._epsilon_min = val

    @property
    def ε_decay(self) -> float:
        """The rate at which the exploration rate decays."""
        return self._epsilon_decay

    @ε_decay.setter
    def ε_decay(self, val: float) -> None:
        """The rate at which the exploration rate decays."""
        self._epsilon_decay = val

    @property
    def decay_frequency(self) -> int:
        """The frequency at which the exploration rate decays."""
        return self._decay_frequency

    @decay_frequency.setter
    def decay_frequency(self, val: int) -> None:
        """The frequency at which the exploration rate decays."""
        self._decay_frequency = val

    @property
    def α(self) -> float:
        """The agent's learning rate."""
        return self._learning_rate

    @α.setter
    def α(self, val: float) -> None:
        """The agent's learning rate."""
        self._learning_rate = val

    @property
    def γ(self) -> float:
        """The agent's discount rate."""
        return self._discount

    @γ.setter
    def γ(self, val: float) -> None:
        """The agent's discount rate."""
        self._discount = val

    def run_step(
        self,
        s: State,
        n: int,
        action_select: ActionSelect,
        should_render: bool = False,
    ) -> Percept:
        """Perform one step within an episode.

        Args:
            s (State): The agent's initial state.
            n (int): The index of the current step.
            action_select (ActionSelect): The action selection strategy.
            should_render (bool, optional): Whether the step should be rendered. Defaults to False.

        Returns:
            Percept: The experienced percept after taking the step.
        """

        # set the initial state
        self._env.state = self._env.unwrapped.state = s

        self._pre_step(s, n)

        ss = self._process_state(s, n)
        a = action_select(ss)
        s_, r, is_terminal, info = self._env.step(a)

        p = self._process_percept(
            Percept(
                s,
                s_,
                a,
                r,
                is_terminal,
                info,
            )
        )

        self._post_step(p, n)

        if should_render:
            self._env.render()

        # Decay epsilon
        if (
            self._is_training
            and self._total_steps % self._decay_frequency == 0
            and self._epsilon > self._epsilon_min
        ):
            self._epsilon *= self._epsilon_decay
            # print("epsilon decayed: {}".format(self._epsilon))

        self._step_number += 1
        self._total_steps += 1

        return p

    def run_episode(
        self,
        stop_condition: EpisodeStopConditionOrLimit,
        action_select: ActionSelect,
        should_render: bool = False,
        should_save: bool = False,
        should_increment: bool = True,
    ) -> Tuple[float, List[Percept]]:
        """Run one entire episode.

        Args:
            stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
            action_select (ActionSelect): The action selection strategy.
            should_render (bool, optional): Whether the step should be rendered. Defaults to False.
            should_save (bool, optional): Whether the model should be saved. Defaults to False.
            should_increment (bool, optional): Whether the episode number should be incremented. Defaults to True.

        Returns:
            Tuple[float, List[Percept]]: The earned total reward and the history of percepts.
        """

        stop_condition = self._get_episode_stop_condition(stop_condition)

        self._step_number = 0

        self._pre_episode()

        s = self._env.reset()
        if should_render:
            self._env.render()

        n = 0
        r = 0.0
        is_terminal = False
        history = []
        for _ in self._episode_generator(
            stop_condition,
            history,
        ):
            n += 1

            p = self.run_step(
                s,
                n,
                action_select,
                should_render,
            )
            r += p.r
            history.append(p)

            is_terminal = p.is_terminal
            if is_terminal:
                break
            s = p.s_new

        if should_increment:
            self._episode_number += 1

        self._post_episode(n, r, history, is_terminal)

        if (
            should_save
            and self._save_file is not None
            and self._episode_number % self._save_frequency == 0
        ):
            self._save_model()

        return r, history

    def fit(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[Percept]]]:
        """Run multiple episodes to train the agent.

        Args:
            run_stop_condition (RunStopConditionOrLimit): A function defining when to stop the run or an episode limit.
            episode_stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
            should_render (bool, optional): Whether the step should be rendered. Defaults to False.
            should_save (bool, optional): Whether the model should be saved. Defaults to False.
            action_select (_type_, optional): The action selection strategy. Defaults to lambda s: None.

        Returns:
            Tuple[float, List[float], List[List[Percept]]]: The total earned reward, the reward history, the episode history.
        """

        self._is_training = True
        self._episode_number = 0

        run_stop_condition = self._get_run_stop_condition(run_stop_condition)

        total_reward = 0.0
        reward_history = []
        episode_history = []
        for _ in tqdm(
            self._run_generator(
                run_stop_condition,
                reward_history,
                episode_history,
            ),
            desc="learning",
            unit="ep",
        ):
            r, ep_history = self.run_episode(
                episode_stop_condition,
                action_select,
                should_render,
                should_save,
            )

            reward_history.append(r)
            episode_history.append(ep_history)
            total_reward += r

        return total_reward, reward_history, episode_history

    def run(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[Percept]]]:
        """Run multiple episodes to test the agent.

        Args:
            run_stop_condition (RunStopConditionOrLimit): A function defining when to stop the run or an episode limit.
            episode_stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
            should_render (bool, optional): Whether the step should be rendered. Defaults to False.
            should_save (bool, optional): Whether the model should be saved. Defaults to False.
            action_select (_type_, optional): The action selection strategy. Defaults to lambdas:None.

        Returns:
            Tuple[float, List[float], List[List[Percept]]]: The total earned reward, the reward history, the episode history.
        """

        self._is_training = False
        self._episode_number = 0

        run_stop_condition = self._get_run_stop_condition(run_stop_condition)

        total_reward = 0.0
        reward_history = []
        episode_history = []
        for _ in tqdm(
            self._run_generator(
                run_stop_condition,
                reward_history,
                episode_history,
            ),
            desc="running",
            unit="ep",
        ):
            r, ep_history = self.run_episode(
                episode_stop_condition,
                action_select,
                should_render,
                should_save,
            )

            reward_history.append(r)
            episode_history.append(ep_history)
            total_reward += r

        return total_reward, reward_history, episode_history

    def _process_state(
        self,
        s: State,
        n: int,
    ) -> State:
        """Process the state before performing action selection.

        Args:
            s (State): The original state.
            n (int): The index of the current step.

        Returns:
            State: The updated state.
        """
        return s

    def _pre_step(
        self,
        s: State,
        n: int,
    ) -> None:
        """A function ran right before performing a step within the environment.

        Args:
            s (State): The original state.
            n (int): The index of the current step.
        """
        pass

    def _post_step(
        self,
        p: Percept,
        n: int,
    ) -> None:
        """A function ran right after performing a step within the environment.

        Args:
            p (Percept): The experienced percept.
            n (int): The index of the current step.
        """
        pass

    def _pre_episode(
        self,
    ) -> None:
        """A function ran right before the start of an episode."""
        pass

    def _post_episode(
        self,
        n: int,
        r: float,
        history: List[Percept],
        is_terminal: bool,
    ) -> None:
        """A function ran right after the completion of an episode.

        Args:
            n (int): The index of the current step.
            r (float): The total earned reward.
            history (List[Percept]): The history of percepts.
            is_terminal (bool): Whether the final state was terminal.
        """
        pass

    def _build_model(self) -> None:
        """Build a new model."""
        pass

    def _save_model(self) -> None:
        """Load a saved model."""
        pass

    def _load_model(self) -> None:
        """Save the model."""
        pass

    def _get_space_shape(self, space: spaces.Space) -> List[int]:
        """Generates a list representing the shape of the given space.

        Args:
            space (spaces.Space): The OpenAI Gym Space object.

        Returns:
            List[int]: The representation of the space.
        """

        if isinstance(space, spaces.Discrete):
            return [space.n]
        elif isinstance(space, spaces.MultiDiscrete):
            return space.nvec
        elif isinstance(space, spaces.MultiBinary):
            return [*space.shape, 2]
        elif isinstance(space, spaces.Box):
            return [*space.shape]
        elif isinstance(space, spaces.Tuple):
            return [self._get_space_shape(s) for s in space.spaces]
        elif isinstance(space, spaces.Dict):
            return [self._get_space_shape(s) for s in space.spaces.values()]

    def _get_run_stop_condition(
        self, stop_condition: RunStopConditionOrLimit
    ) -> RunStopCondition:
        """Converts an integer limit to a run stop condition function.

        Args:
            stop_condition (RunStopConditionOrLimit): A function defining when to stop the run or an episode limit.

        Returns:
            RunStopCondition: A function defining when to stop the run.
        """

        if isinstance(stop_condition, int):
            step_limit = stop_condition
            stop_condition = lambda n, r_hist, ep_hist: n >= step_limit
        return stop_condition

    def _get_episode_stop_condition(
        self, stop_condition: EpisodeStopConditionOrLimit
    ) -> EpisodeStopCondition:
        """Converts an integer limit to an episode stop condition function.

        Args:
            stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop the episode or a step limit.

        Returns:
            EpisodeStopCondition: A function defining when to stop the episode.
        """

        if isinstance(stop_condition, int):
            step_limit = stop_condition
            stop_condition = lambda n, history: n >= step_limit
        return stop_condition

    def _run_generator(
        self,
        stop_condition: RunStopCondition,
        reward_history: List[float],
        episode_history: List[List[Percept]],
    ):
        """An asynchronous generator for the agent's runs. Primarily used for tqdm.

        Args:
            stop_condition (RunStopCondition): A function defining when to stop the run.
            reward_history (List[float]): A chronological list of earned rewards.
            episode_history (List[List[Percept]]): A chronological list of experienced episodes.
        """

        while not stop_condition(self._episode_number, reward_history, episode_history):
            yield

    def _episode_generator(
        self,
        stop_condition: EpisodeStopCondition,
        history: List[Percept],
    ):
        """An asynchronous generator for the episodes. Primarily used for tqdm.

        Args:
            stop_condition (EpisodeStopCondition): A function defining when to stop the episode.
            history (List[Percept]): A chronological list of experienced episodes.
        """

        while not stop_condition(self._step_number, history):
            yield


class DeepAgent(Agent):
    """Represents a generic Deep Reinforcement Learning Agent."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        update_target_frequency: int,
        batch_size: int,
        memory_limit: int,
        model_builder: ModelBuilder,
        model_predictor: ModelPredictor,
        model_fitter: ModelFitter,
        model_copier: ModelCopier,
        model_loader: ModelLoader,
        model_saver: ModelSaver,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        self._model = None
        self._model_target = None
        self._update_target_frequency = update_target_frequency

        self._model_builder = model_builder
        self._model_predictor = model_predictor
        self._model_fitter = model_fitter
        self._model_copier = model_copier

        self._model_loader = model_loader
        self._model_saver = model_saver

        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            save_file,
            process_percept,
            cold_start,
        )

        self._replay_buffer = PrioritizedReplayBuffer(memory_limit, batch_size)

        if self._model is None or self._model_target is None:
            self._build_model()

    def _parse_state(self, s: State) -> State:
        """Parse the given state and convert it to a list.

        Args:
            s (State): The original state

        Returns:
            State: The parsed state.
        """

        if not (
            isinstance(s, tuple)
            or isinstance(s, list)
            or isinstance(s, np.ndarray)
            or isinstance(s, tf.Tensor)
        ):
            s = [1 if i == s else 0 for d in self._n_states for i in range(d)]
        return s

    def _build_model(self) -> None:
        self._model = self._model_builder(self._n_states, self._n_actions)
        self._model_target = self._model_builder(self._n_states, self._n_actions)

    def _load_model(self) -> None:
        self._model = self._model_loader(self._save_file)
        self._model_target = self._model_loader(self._save_file)

    def _save_model(self) -> None:
        self._model_saver(self._model, self._save_file)

    def _memorize(self, p: Percept) -> None:
        """Memorize an encountered percept.

        Args:
            p (Percept): The percept to memorize.
        """

        self._replay_buffer.store(p)

    def _get_memory_batch(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Get a batch sample from the replay buffer.

        Returns:
            Tuple[np.ndarray, np.ndarray, np.ndarray]: List of indices, list of TD errors, and the batch samples.
        """

        return self._replay_buffer.sample()

    def _get_training_data(
        self,
        S: List[State],
        A: List[Action],
        R: List[float],
        S_: List[State],
        T: List[bool],
    ) -> Tuple[List[Action], List[Action]]:
        """Collect the training data.

        Args:
            S (List[State]): The list of previous states.
            A (List[Action]): The list of taken actions.
            R (List[float]): The list of earned rewards.
            S_ (List[State]): The list of landed states.
            T (List[bool]): The list of terminal flags.

        Returns:
            Tuple[List[Action], List[Action]]: The training samples and actual output samples.
        """

        Y: np.ndarray = np.array(self._model_predictor(self._model_target, S_))
        Y_train: np.ndarray = np.array(self._model_predictor(self._model, S))

        Q = np.amax(Y, 1)
        Y_train[np.arange(len(Y_train)), A] = R + self._discount * Q * (1 - T)

        return Y_train, Y

    def _update(self, n: int) -> None:
        """Update the model using the memorized percepts.

        Args:
            n (int): The index of the current step.
        """

        if len(self._replay_buffer) < self._replay_buffer.batch_size:
            return

        idxs, weights, minibatch = self._get_memory_batch()

        # sample memory
        S = np.array([self._parse_state(p.s) for p in minibatch])
        S_ = np.array([self._parse_state(p.s_new) for p in minibatch])
        R = np.array([p.r for p in minibatch])
        A = np.array([p.a for p in minibatch])
        T = np.array([float(p.is_terminal) for p in minibatch])

        Y_train, Y = self._get_training_data(S, A, R, S_, T)

        while Y_train.shape != Y.shape:
            Y_train = Y_train[-1]

        loss = lambda X, Y: np.repeat(
            np.expand_dims(weights, axis=1), X.shape[1], axis=1
        ) * huber(X, Y)
        self._model = self._model_fitter(self._model, S, Y_train, loss)

        Y_idx = Y_train.argmax(1)
        rows = np.arange(len(Y_idx))
        self._replay_buffer.update(rows, Y_train[rows, Y_idx] - Y[rows, Y_idx])
        self._update_target()

    def _update_target(self):
        """Update the target model."""

        if (self._total_steps % self._update_target_frequency) == 0:
            self._model_target = self._model_copier(self._model_target, self._model)


class DoubleDeepAgent(DeepAgent):
    """Represents a generic Double Deep Reinforcement Learning Agent."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        update_target_frequency: int,
        batch_size: int,
        memory_limit: int,
        model_builder: ModelBuilder,
        model_predictor: ModelPredictor,
        model_fitter: ModelFitter,
        model_copier: ModelCopier,
        model_loader: ModelLoader,
        model_saver: ModelSaver,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            update_target_frequency,
            batch_size,
            memory_limit,
            model_builder,
            model_predictor,
            model_fitter,
            model_copier,
            model_loader,
            model_saver,
            save_file,
            process_percept,
            cold_start,
        )

    def _get_training_data(
        self,
        S: List[State],
        A: List[Action],
        R: List[float],
        S_: List[State],
        T: List[bool],
    ) -> Tuple[List[Action], List[Action]]:
        Y: np.ndarray = np.array(self._model_predictor(self._model_target, S_))
        Y_train: np.ndarray = np.array(self._model_predictor(self._model, S))
        A_: np.ndarray = np.array(self._model_predictor(self._model, S_))

        Q = Y[np.arange(len(Y)), np.argmax(A_, 1)]
        Y_train[np.arange(len(Y_train)), A] = R + self._discount * Q * (1 - T)

        return Y_train, Y
