"""
Author: Nick Van Osta
"""


import gzip
import os
from random import getstate
from typing import Any, Callable, List, Tuple

import numpy as np
from tqdm import tqdm
from agent import (
    ActionSelect,
    Agent,
    ProcessPercept,
    EpisodeStopConditionOrLimit,
    RunStopConditionOrLimit,
)
from gym import Env

import neat

try:
    import cPickle as pickle  # pylint: disable=import-error
except ImportError:
    import pickle  # pylint: disable=import-error


def save_checkpoint(
    self,
    config,
    population,
    species_set,
    generation,
):
    """Save the current simulation state.

    Args:
        config (neat.Config): The NEAT configuration.
        population (neat.Population): The NEAT population.
        species_set (neat.DefaultSpeciesSet): The NEAT species set.
        generation (int): The index of the generation.
    """

    filename = "{0}{1}".format(self.filename_prefix, generation)

    with gzip.open(filename, "w+", compresslevel=5) as f:
        data = (generation, config, population, species_set, getstate())
        pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)


# Remove print statement from save_checkpoint method
neat.Checkpointer.save_checkpoint = save_checkpoint


class NEATAgent(Agent):
    """Represents a neuro-evolutionary Agent using the NEAT algorithm."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        config_file: str,
        generations_per_episode: int,
        save_frequency: int,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        self.__config = neat.Config(
            neat.DefaultGenome,
            neat.DefaultReproduction,
            neat.DefaultSpeciesSet,
            neat.DefaultStagnation,
            config_file,
        )
        self.__generations_per_episode = generations_per_episode

        self.__current_generation = 0
        self.__checkpointer = neat.Checkpointer(filename_prefix=save_file + "/")

        self.__winner = None

        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            save_file,
            process_percept,
            cold_start,
        )

    def fit(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[float]]]:
        self._is_training = True
        self._episode_number = 0

        run_stop_condition = self._get_run_stop_condition(run_stop_condition)

        total_reward = 0.0
        reward_history = []
        episode_history = []
        for _ in tqdm(
            self._run_generator(
                run_stop_condition,
                reward_history,
                episode_history,
            )
        ):
            self.__winner = self.__pop.run(
                self.__fitness(episode_stop_condition, False, should_save),
                self.__generations_per_episode,
            )
            self._episode_number += 1
            self.__current_generation += self.__generations_per_episode

            # only render winner, if necessary
            if should_render:
                fitness = self.__fitness(episode_stop_condition, should_render, False)
                fitness([(self.__winner.key, self.__winner)], self.__config)

            reward_history.append(self.__winner.fitness)
            episode_history.append(self.__winner.ep_history)
            total_reward += self.__winner.fitness

        return total_reward, reward_history, episode_history

    def run(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[float]]]:
        winner_net = neat.nn.FeedForwardNetwork.create(self.__winner, self.__config)
        action_select = self._action_select(winner_net)

        return Agent.run(
            self,
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            action_select,
        )

    def _build_model(self) -> None:
        self.__pop = neat.Population(self.__config)
        self.__winner = None

    def _save_model(self) -> None:
        if not os.path.exists(self._save_file):
            os.makedirs(self._save_file)

        self.__checkpointer.save_checkpoint(
            self.__config,
            self.__pop.population,
            self.__pop.species,
            self.__current_generation,
        )

    def _load_model(self) -> None:
        i = 0
        while os.path.exists(self._save_file + "/" + str(i)):
            i += self._save_frequency
        i -= self._save_frequency

        if os.path.exists(self._save_file + "/" + str(i)):
            self.__pop = self.__checkpointer.restore_checkpoint(
                self._save_file + "/" + str(i)
            )

            self.__winner = neat.nn.FeedForwardNetwork.create(
                self.__pop.best_genome
                if self.__pop.best_genome is not None
                else self.__pop.population[0],
                self.__config,
            )
        else:
            self._build_model()

    def _action_select(self, net: neat.nn.FeedForwardNetwork) -> ActionSelect:
        """Get the appropriate action selection method for the given NEAT network.

        Args:
            net (neat.nn.FeedForwardNetwork): The NEAT network to generate the action selection strategy.

        Returns:
            ActionSelect: The action selection strategy.
        """

        return (
            lambda s: np.argmax(net.activate(s))
            if self.env_is_discrete
            else lambda s: net.activate(s) * self.env.action_space.high
        )

    def __fitness(
        self,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
    ) -> Callable[[List[Any], Any], None]:
        """Generates the fitness function.

        Args:
            episode_stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
            should_render (bool, optional): Whether the episode should be rendered. Defaults to False.
            should_save (bool, optional): Whether the model should be saved. Defaults to False.

        Returns:
            Callable[[List[neat.DefaultGenome], neat.Config], None]: The generated fitness function.
        """

        episode_stop_condition = self._get_episode_stop_condition(
            episode_stop_condition
        )

        def fitness_function(
            genomes: List[neat.DefaultGenome], config: neat.Config
        ) -> None:
            for _, genome in genomes:
                net = neat.nn.FeedForwardNetwork.create(genome, config)
                genome.fitness, genome.ep_history = self.run_episode(
                    episode_stop_condition,
                    self._action_select(net),
                    should_render,
                    should_save,
                    False,
                )

        return fitness_function
