# Neuro-Evolutionary Deep Reinforcement Learning

## `agent/neuroevolution`

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

This folder contains the Python 3 code for the `agent.neuroevolution` module.
This module handles everything to do with the implemented Neuro-Evolution Agents.

## Citing

When citing anything from the source code, please cite it as follows:

```bibtex
@mastersthesis{vanosta:2022,
  author = {Van Osta, Nick},
  title  = {Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control},
  school = {University Ghent},
  year   = {2022}
}
```

## Outline

### Files

- [`__init__.py`](./__init__.py): Contains the code for the `agent.neuroevolution` module, mostly defines basic types and classes.
- [`neat.py`](./neat.py): Contains an implementation for the NEAT agent.
