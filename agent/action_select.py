"""
Author: Nick Van Osta
"""


import numpy as np
import numpy.typing as npt
from gym.core import Env

from agent import ActionSelect, Agent


def random_sample(env: Env) -> ActionSelect:
    """Randomly select an action from the environment's set of actions.

    Args:
        env (Env): The environment to sample the action from.

    Returns:
        ActionSelect: The action selection strategy.
    """

    return lambda s: env.action_space.sample()


def by_policy(policy: npt.NDArray, *, precision: float = 0.1e-9) -> ActionSelect:
    """Select the next action by following the given policy.

    Args:
        policy (npt.NDArray): The agent's policy.
        precision (float, optional): Precision used for float equality. Defaults to 0.1e-9.

    Returns:
        ActionSelect: The action selection strategy.
    """

    return lambda s: np.random.choice(
        [
            i
            for (p, i) in enumerate(policy[s])
            if np.fabs(p - np.max(policy[s])) < precision
        ]
    )


def epsilon_greedy(
    agent: Agent, explore: ActionSelect, exploit: ActionSelect
) -> ActionSelect:
    """Select the next action with a randomly assigned method dependent on the agent's ε.

    - High ε: encourages exploration
    - Low ε: encourages exploitation

    Args:
        agent (Agent): The agent to apply epsilon-greedy on.
        explore (ActionSelect): The exploration action selection strategy.
        exploit (ActionSelect): The exploitation action selection strategy.

    Returns:
        ActionSelect: The action selection strategy.
    """

    return lambda s: explore(s) if np.random.uniform(0, 1) < agent.ε else exploit(s)
