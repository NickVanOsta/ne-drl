"""
Author: Nick Van Osta
"""


from typing import List
import numpy as np


def mse(X: List[float], Y: List[float]) -> List[float]:
    """The Mean Squared Error loss function.

    Args:
        X (List[float]): The list of first values.
        Y (List[float]): The list of second values.

    Returns:
        List[float]: The list of errors.
    """

    return np.mean([(x - y) ** 2 for x, y in zip(X, Y)])


def mae(X: List[float], Y: List[float]) -> List[float]:
    """The Mean Absolute Error loss function.

    Args:
        X (List[float]): The list of first values.
        Y (List[float]): The list of second values.

    Returns:
        List[float]: The list of errors.
    """

    return np.mean([abs(x - y) for x, y in zip(X, Y)])


def huber(X: List[float], Y: List[float], delta=1) -> List[float]:
    """The Huber loss function, which applies MSE for low values, and MAE for higher values.

    Args:
        X (List[float]): The list of first values.
        Y (List[float]): The list of second values.
        delta (int, optional): The delta parameter that defines the border between MSE and MAE. Defaults to 1.

    Returns:
        List[float]: The list of errors.
    """

    a = X - Y
    return 0.5 * a ** 2 * (abs(a) <= delta) + delta * (abs(a) - 0.5 * delta) * (
        abs(a) > delta
    )
