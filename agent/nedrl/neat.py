"""
Author: Nick Van Osta
"""


import gzip
import os
from copy import deepcopy
from random import getstate
from typing import Any, Callable, List

import numpy as np
from agent import (
    Action,
    ActionSelect,
    LossFunction,
    ProcessPercept,
    State,
)
from agent.nedrl import NEDRLAgent
from gym import Env

import neat

try:
    import cPickle as pickle  # pylint: disable=import-error
except ImportError:
    import pickle  # pylint: disable=import-error


def save_checkpoint(
    self,
    config: neat.Config,
    population: neat.Population,
    species_set: neat.DefaultSpeciesSet,
    generation: int,
):
    """Save the current simulation state.

    Args:
        config (neat.Config): The NEAT configuration.
        population (neat.Population): The NEAT population.
        species_set (neat.DefaultSpeciesSet): The NEAT species set.
        generation (int): The index of the generation.
    """

    filename = "{0}{1}".format(self.filename_prefix, generation)

    with gzip.open(filename, "w+", compresslevel=5) as f:
        data = (generation, config, population, species_set, getstate())
        pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)


# Remove print statement from save_checkpoint method
neat.Checkpointer.save_checkpoint = save_checkpoint


class NEATDRLAgent(NEDRLAgent):
    """Represents a Neuro-Evolutionary Deep Reinforcement Learning Agent using the NEAT algorithm."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        update_target_frequency: int,
        batch_size: int,
        memory_limit: int,
        config_file: str,
        generations_per_update: int,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        self._config = neat.Config(
            neat.DefaultGenome,
            neat.DefaultReproduction,
            neat.DefaultSpeciesSet,
            neat.DefaultStagnation,
            config_file,
        )
        self._pop = None
        self._checkpointer = neat.Checkpointer(filename_prefix=save_file + "/")
        self._best_model = None

        # ! gym update broke this
        # self._env_cpy = deepcopy(env)

        self.fitness_history = []

        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            update_target_frequency,
            batch_size,
            memory_limit,
            self._model_builder,
            self._model_predictor,
            self._model_fitter,
            self._model_copier,
            self._model_loader,
            self._model_saver,
            generations_per_update,
            save_file,
            process_percept,
            cold_start,
        )

    def _model_builder(self, n_states: int, n_actions: int) -> neat.DefaultGenome:
        """Build a model using NEAT.

        Args:
            n_states (int): The amount of states.
            n_actions (int): The amount of actions.

        Returns:
            neat.DefaultGenome: The built NEAT model.
        """

        self._pop = neat.Population(self._config)
        return self._pop.population[1]

    def _model_predictor(self, model: neat.DefaultGenome, S: State) -> List[Action]:
        """Predict values using the given model.

        Args:
            model (neat.DefaultGenome): The NEAT model used for value prediction.
            S (State): The state or list of states to be used as prediction input.

        Returns:
            List[Action]: The list of predicted actions.
        """

        net = neat.nn.FeedForwardNetwork.create(
            model,
            self._config,
        )

        if not (
            isinstance(S, tuple) or isinstance(S, list) or isinstance(S, np.ndarray)
        ):
            S = [[+(s == S) for s in range(len(net.input_nodes))]]

        if len(net.input_nodes) == len(S):
            Y = np.array([net.activate(S)])
        else:
            Y = np.array([net.activate(s) for s in S])

        if np.prod(self._n_actions) == len(net.output_nodes):
            return Y

        a = Y[:, 1:]
        v = np.tile(Y[:, :1], (1, a.shape[1]))
        q = v + a - np.tile(a.mean(1, keepdims=True), (1, a.shape[1]))

        return q

    def _model_fitter(
        self,
        model: neat.DefaultGenome,
        s: State,
        a: Action,
        L: LossFunction,
    ) -> neat.DefaultGenome:
        """Fit the model.

        Args:
            model (neat.DefaultGenome): The NEAT model to be fitted.
            s (State): The inputted state.
            a (Action): The expected action.
            L (LossFunction): The applied loss function.

        Returns:
            neat.DefaultGenome: The fitted model.
        """

        for g in range(self._generations_per_update):
            winner = self._pop.run(
                self._fitness(s, a, L),
                1,
            )
            self.fitness_history.append(winner.fitness)

        return winner

    def _model_copier(
        self, target: neat.DefaultGenome, model: neat.DefaultGenome
    ) -> neat.DefaultGenome:
        """Copies the given model.

        Args:
            target (neat.DefaultGenome): The copy target.
            model (neat.DefaultGenome): The copy source.

        Returns:
            neat.DefaultGenome: The updated copy target.
        """

        return deepcopy(model)

    def _model_loader(self, save_file: str) -> neat.DefaultGenome:
        """Load a previously saved model.

        Args:
            save_file (str): The path to the saved model.

        Raises:
            RuntimeError: When no checkpoint saves are found.

        Returns:
            neat.DefaultGenome: The loaded model.
        """

        i = 1
        while os.path.exists(save_file + "/" + str(i)):
            i += self._save_frequency
        i -= self._save_frequency
        save_file = save_file + "/" + str(i)

        if os.path.exists(save_file):
            self._pop = self._checkpointer.restore_checkpoint(save_file)
        else:
            raise RuntimeError("No checkpoint file found")

        return self._pop.run(
            self._rl_fitness(),
            1,
        )

    def _model_saver(self, model: neat.DefaultGenome, save_file: str) -> None:
        """Save the model.

        Args:
            model (neat.DefaultGenome): The model to save.
            save_file (str): The destination file or directory to save to.
        """

        if not os.path.exists(save_file):
            os.makedirs(save_file)

        self._checkpointer.save_checkpoint(
            self._config,
            self._pop.population,
            self._pop.species,
            self._episode_number,
        )

    def _rl_action_select(self, net: neat.DefaultGenome) -> ActionSelect:
        """The action selection strategy for standard NEAT solutions.

        Args:
            net (neat.DefaultGenome): The NEAT generated neural net.

        Returns:
            ActionSelect: The action selection strategy.
        """

        return (
            lambda s: np.argmax(net.activate(s))
            if self.env_is_discrete
            else lambda s: net.activate(s) * self.env.action_space.high
        )

    def _rl_fitness(self):
        """The fitness function for standard NEAT solutions."""

        def fitness_function(
            genomes: List[neat.DefaultGenome], config: neat.Config
        ) -> float:
            for _, genome in genomes:
                self.__rl_fitness_episode(genome, config)

        return fitness_function

    def __rl_fitness_episode(
        self, genome: neat.DefaultGenome, config: neat.Config
    ) -> float:
        """The fitness function for 1 episode of a standard NEAT solution.

        Args:
            genome (neat.DefaultGenome): The NEAT genome.
            config (neat.Config): The NEAT configuration.

        Returns:
            float: The sum of differences in earned reward.
        """

        was_training = self._is_training
        step_number = self._step_number
        env = self._env

        self._is_training = False
        # self._env = self._env_cpy

        net = neat.nn.FeedForwardNetwork.create(genome, config)
        _, history = self.run_episode(
            2, self._rl_action_select(net), False, False, False
        )

        self._is_training = was_training
        self._step_number = step_number
        self._env = env

        return sum([history[i + 1].r - history[i].r for i in range(len(history) - 1)])

    def _fitness(
        self,
        initial_state: State,
        expected_action: Action,
        L: LossFunction,
    ) -> Callable[[List[Any], Any], None]:
        """The NEAT-DRL fitness function.

        Args:
            initial_state (State): The inputted state.
            expected_action (Action): The expected action.
            L (LossFunction): The applied loss function.

        Returns:
            Callable[[List[neat.DefaultGenome], neat.Config], None]: The fitness function.
        """

        def fitness_function(genomes: List[neat.DefaultGenome], config: neat.Config):
            for _, genome in genomes:
                net = neat.nn.FeedForwardNetwork.create(genome, config)

                Y = np.array([net.activate(S)[1:] for S in initial_state])
                genome.fitness = -np.sum(L(expected_action, Y))

        return fitness_function
