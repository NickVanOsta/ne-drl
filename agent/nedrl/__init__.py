"""
Author: Nick Van Osta
"""


from typing import List, Tuple

import numpy as np
from agent import (
    Action,
    ActionSelect,
    DeepAgent,
    EpisodeStopConditionOrLimit,
    Percept,
    ProcessPercept,
    RunStopConditionOrLimit,
    State,
)
from agent.action_select import epsilon_greedy, random_sample
from environment import (
    ModelBuilder,
    ModelCopier,
    ModelFitter,
    ModelLoader,
    ModelPredictor,
    ModelSaver,
)
from gym import Env


class NEDRLAgent(DeepAgent):
    """Represents a generic Neuro-Evolutionary Deep Reinforcement Learning Agent."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        update_target_frequency: int,
        batch_size: int,
        memory_limit: int,
        model_builder: ModelBuilder,
        model_predictor: ModelPredictor,
        model_fitter: ModelFitter,
        model_copier: ModelCopier,
        model_loader: ModelLoader,
        model_saver: ModelSaver,
        generations_per_update: int = None,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        self._generations_per_update = generations_per_update
        self._previous_model_target = None

        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            update_target_frequency,
            batch_size,
            memory_limit,
            model_builder,
            model_predictor,
            model_fitter,
            model_copier,
            model_loader,
            model_saver,
            save_file,
            process_percept,
            cold_start,
        )

    def _build_model(self) -> None:
        self._model = self._model_builder(self._n_states, self._n_actions)
        self._model_target = self._model_builder(self._n_states, self._n_actions)
        self._previous_model_target = self._model_builder(
            self._n_states, self._n_actions
        )

    def _load_model(self) -> None:
        self._model = self._model_loader(self._save_file)
        self._model_target = self._model_loader(self._save_file)
        self._previous_model_target = self._model_loader(self._save_file)

    def _action_select(self) -> ActionSelect:
        """Get the appropriate action selection method depending on the environment type.

        Returns:
            ActionSelect: The action selection strategy.
        """

        exploit = (
            (lambda s: np.argmax(self._model_predictor(self._model, s)))
            if self.env_is_discrete
            else (lambda s: self._model_predictor(self._model, s))
            * self.env.action_space.high
        )
        explore = random_sample(self._env)

        return epsilon_greedy(self, explore, exploit) if self._is_training else exploit

    def fit(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[float]]]:
        self._is_training = True

        return super().fit(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            self._action_select(),
        )

    def run(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
        action_select: ActionSelect = lambda s: None,
    ) -> Tuple[float, List[float], List[List[float]]]:
        self._is_training = False

        return super().run(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            self._action_select(),
        )

    def _post_step(
        self,
        p: Percept,
        n: int,
    ) -> None:
        if self._is_training:
            self._memorize(p)
            self._update(n)

    def _get_training_data(
        self,
        S: List[State],
        A: List[Action],
        R: List[float],
        S_: List[State],
        T: List[bool],
    ) -> Tuple[np.ndarray, np.ndarray]:
        z = (
            1.0
            * (self._total_steps % self._update_target_frequency)
            / self._update_target_frequency
        )

        Y_0: np.ndarray = np.array(
            self._model_predictor(self._previous_model_target, S_)
        )
        Y: np.ndarray = np.array(self._model_predictor(self._model_target, S_))
        Y_train: np.ndarray = np.array(self._model_predictor(self._model, S))
        A_ = self._model_predictor(self._model, S_)

        Q_0 = Y_0[np.arange(len(Y_0)), np.argmax(A_, 1)]
        Q_1 = Y[np.arange(len(Y)), np.argmax(A_, 1)]
        Q = (1 - np.tile(z, T.shape)) * Q_0 + np.tile(z, T.shape) * Q_1
        Y_train[np.arange(len(Y_train)), A] = R + self._discount * Q * (1 - T)

        # if self._previous_target is None:
        #     self._previous_target = Y_train.copy()
        # Z = np.linspace(0, 1, self._generations_per_update)
        # Y_train_interp = np.array(
        #     [(1 - z) * self._previous_target + z * Y_train for z in Z]
        # )
        # self._previous_target = Y_train.copy()

        return Y_train, Y

    def _update_target(self):
        if (self._total_steps % self._update_target_frequency) == 0:
            self._previous_model_target = self._model_copier(
                self._previous_model_target, self._model_target
            )
            self._model_target = self._model_copier(self._model_target, self._model)
