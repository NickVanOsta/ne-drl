"""
Author: Nick Van Osta
"""


import gzip
import pickle
from typing import Any, List, Tuple, Union

import numpy as np
import tensorflow as tf
from agent import (
    Action,
    EpisodeStopConditionOrLimit,
    Percept,
    ProcessPercept,
    RunStopConditionOrLimit,
    State,
)
from agent.q_learning import QFunc, QLearningAgent
from gym import Env, spaces


class TabularQLearningAgent(QLearningAgent):
    """Represents a Q-Learning Agent."""

    GZIP_COMPRESS_LEVEL = 5

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        float_min: float,
        float_max: float,
        float_samples: float,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            save_file,
            process_percept,
            cold_start,
        )
        states = self._n_states
        actions = self._n_actions

        self.__states = None
        self.__actions = None

        if isinstance(env.observation_space, spaces.Box):
            self.__states = tf.constant(
                [
                    [
                        i
                        for i in np.linspace(
                            max(env.observation_space.low[s], float_min),
                            min(env.observation_space.high[s], float_max),
                            float_samples,
                        )
                    ]
                    for s in range(env.observation_space.shape[-1])
                ],
            )
            states = (
                *self._n_states[:-1],
                *[float_samples for _ in range(self._n_states[-1])],
            )

        if isinstance(env.action_space, spaces.Box):
            self.__actions = tf.constant(
                [
                    [
                        i
                        for i in np.linspace(
                            max(env.action_space.low[s], float_min),
                            min(env.action_space.high[s], float_max),
                            float_samples,
                        )
                    ]
                    for s in range(env.observation_space.shape[-1])
                ]
            )
            actions = (
                *self._n_actions[:-1],
                *[float_samples for _ in range(self._n_actions[-1])],
            )

        if not hasattr(self, "__q") or self.__q.shape != (*states, *actions):
            self.__q = np.random.uniform(size=(*states, *actions))

    def _save_model(self) -> None:
        with gzip.open(
            self._save_file, "w+", compresslevel=self.GZIP_COMPRESS_LEVEL
        ) as f:
            pickle.dump(self.__q, f, protocol=pickle.HIGHEST_PROTOCOL)

    def _load_model(self) -> None:
        try:
            with gzip.open(
                self._save_file, "r", compresslevel=self.GZIP_COMPRESS_LEVEL
            ) as f:
                self.__q = pickle.load(f)
        except:
            pass

    def _process_state(self, s: State, n: int) -> State:
        return self.__to_indices(self.__states, s)

    def _Q(self, s: State = None, a: Action = None) -> float:
        q = self.__q
        if (
            isinstance(s, list)
            or isinstance(s, np.ndarray)
            or isinstance(s, tf.Tensor)
            or s is not None
        ):
            q = self.__q[tuple(s)]

        if (
            isinstance(a, list)
            or isinstance(a, np.ndarray)
            or isinstance(a, tf.Tensor)
            or a is not None
        ):
            q = q[tuple(a)]

        return q

    def _update_Q(
        self,
        p: Percept,
        n: int,
    ):
        s, a, ss = self.__get_state_action(p)
        t = (*s, *a)
        self.__q[tuple(t)] = (1 - self._learning_rate) * self.__q[
            tuple(t)
        ] + self._learning_rate * (p.r + self._discount * np.max(self.__q[ss]))

    def __to_indices(self, lst: List[Any], x: Union[State, Action]) -> List[int]:
        """Maps the given value `x` to an index within `lst`.

        Args:
            lst (List[Any]): The list of values.
            x (Union[State, Action]): The value(s) to search for.

        Returns:
            List[int]: The list of indices.
        """

        if isinstance(x, list) or isinstance(x, np.ndarray) or isinstance(x, tf.Tensor):
            y = [0 for _ in x]
        else:
            y = [0]

        if lst is not None:
            for i, ss in enumerate(lst):
                for j, s in enumerate(ss):
                    if s > y[i]:
                        y[i] = j
                        break

            y = np.clip(y, 0, [len(ss) - 1 for ss in lst])

        return y

    def __get_state_action(self, p: Percept) -> Tuple[State, Action, State]:
        """Map the given percept to indices.

        Args:
            p (Percept): An experienced percept.

        Returns:
            Tuple[State, Action, State]: The table indices of the states and action.
        """

        return (
            self.__to_indices(self.__states, p.s),
            self.__to_indices(self.__actions, p.a),
            self.__to_indices(self.__states, p.s_new),
        )
