"""
Author: Nick Van Osta
"""


from gym import Env
import numpy as np

from typing import Callable, List, Tuple

from agent import (
    Action,
    ActionSelect,
    Agent,
    EpisodeStopConditionOrLimit,
    Percept,
    ProcessPercept,
    RunStopConditionOrLimit,
    State,
)
from agent.action_select import epsilon_greedy, random_sample


QFunc = Callable[[State, Action], float]


def by_q_value(Q: QFunc) -> ActionSelect:
    """Select the next action by following the given Q-table.

    Args:
        Q (QFunc): The Q-function to use.

    Returns:
        ActionSelect: The action selection strategy.
    """

    return lambda s: np.argmax(Q(s))


class QLearningAgent(Agent):
    """Represents a Q-Learning Agent."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        super().__init__(
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            save_file,
            process_percept,
            cold_start,
        )

    @property
    def Q(self) -> QFunc:
        """The function that calculates the Q-value for a certain state-action pair."""

        return self._Q

    def fit(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
    ) -> Tuple[float, List[float], List[List[float]]]:
        return super().fit(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            epsilon_greedy(self, random_sample(self._env), by_q_value(self._Q)),
        )

    def run(
        self,
        run_stop_condition: RunStopConditionOrLimit,
        episode_stop_condition: EpisodeStopConditionOrLimit,
        should_render: bool = False,
        should_save: bool = False,
    ) -> Tuple[float, List[float], List[List[float]]]:
        return super().run(
            run_stop_condition,
            episode_stop_condition,
            should_render,
            should_save,
            by_q_value(self._Q),
        )

    def _post_step(
        self,
        p: Percept,
        n: int,
    ) -> None:
        if self._is_training:
            self._update_Q(p, n)

    def _Q(self, s: State, a: Action) -> float:
        """Retrieve the Q-value for a certain state-action pair from the current Q-function approximation.

        Args:
            s (State): The state.
            a (Action): The action.

        Raises:
            NotImplemented: When the function is not overriden.

        Returns:
            float: The action value.
        """

        raise NotImplemented

    def _update_Q(
        self,
        p: Percept,
        n: int,
    ):
        """Update the Q-function approximation.

        Args:
            p (Percept): An experienced percept.
            n (int): The index of the current step.

        Raises:
            NotImplemented: When the function is not overriden.
        """

        raise NotImplemented
