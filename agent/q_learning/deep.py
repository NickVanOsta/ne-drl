"""
Author: Nick Van Osta
"""


from typing import List, Union

from agent import (
    Action,
    DoubleDeepAgent,
    Percept,
    ProcessPercept,
    State,
)
from agent.q_learning import QLearningAgent
from environment import (
    ModelBuilder,
    ModelCopier,
    ModelFitter,
    ModelLoader,
    ModelPredictor,
    ModelSaver,
)
from gym.core import Env


class DeepQLearningAgent(DoubleDeepAgent, QLearningAgent):
    """Represents a Deep Q-Learning Agent."""

    def __init__(
        self,
        env: Env,
        epsilon_min: float,
        epsilon_max: float,
        epsilon_decay: float,
        decay_frequency: int,
        learning_rate: float,
        discount: float,
        save_frequency: int,
        update_target_frequency: int,
        batch_size: int,
        memory_limit: int,
        model_builder: ModelBuilder,
        model_predictor: ModelPredictor,
        model_fitter: ModelFitter,
        model_copier: ModelCopier,
        model_loader: ModelLoader,
        model_saver: ModelSaver,
        save_file: str = None,
        process_percept: ProcessPercept = lambda p: p,
        cold_start: bool = False,
    ) -> None:
        DoubleDeepAgent.__init__(
            self,
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            update_target_frequency,
            batch_size,
            memory_limit,
            model_builder,
            model_predictor,
            model_fitter,
            model_copier,
            model_loader,
            model_saver,
            save_file,
            process_percept,
            cold_start,
        )

        QLearningAgent.__init__(
            self,
            env,
            epsilon_min,
            epsilon_max,
            epsilon_decay,
            decay_frequency,
            learning_rate,
            discount,
            save_frequency,
            save_file,
            process_percept,
            cold_start,
        )

    def _update_Q(
        self,
        p: Percept,
        n: int,
    ):
        self._memorize(p)

        # update model
        self._update(n)

    def _Q(self, s: State, a: Action = None) -> Union[float, List[float]]:
        s = self._parse_state(s)
        y = self._model_predictor(self._model, s)

        if a == None:
            return y
        return y[:, a]
