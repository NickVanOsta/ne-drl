# Neuro-Evolutionary Deep Reinforcement Learning

## `.githooks`

This folder contains the git hooks updated by Git LFS, plus some additional custom hooks to check for correct commit messages and formatted documents.
The files in this folder should not be changed unless absolutely necessary.
