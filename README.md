# Neuro-Evolutionary Deep Reinforcement Learning

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Version: **v1.0.2**

Welcome to the code repository for my master's dissertation:
Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control.
This repository contains the source code for my implemented agents,
RL environments, configuration, and logs containing test data.
Additionally, there are Jupyter notebooks available that contain
experiments and their results.

## Citing

When citing anything from the source code or thesis, please cite it as follows:

```bibtex
@mastersthesis{vanosta:2022,
  author = {Van Osta, Nick},
  title  = {Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control},
  school = {University Ghent},
  year   = {2022}
}
```

## Initial Setup

Upon cloning this project for the first time, run the following command:

```sh
make init
```

This will initialize the Python Virtual Environment and sets up the Git Hooks.
After that run the following to activate the Virtual Environment and install the dependencies:

```sh
source .venv/bin/activate
make install
```

After that you can run the project by executing the following command:

```sh
make run
```

Alternatively, if your environment doesn't support Make:

```sh
git config core.hooksPath .githooks
python3 -m venv .venv

source .venv/bin/activate
pip install -r requirements.txt

datetime=$(shell date +"%Y%m%d%H%M%S")
mkdir -p "logs/$(datetime)/"
python main.py "logs/$(datetime)/" | tee "logs/$(datetime)/output.log"
```

## Outline

### Folders

- [`__data__`](./__data__/): Contains configuration and save data for the various agents.
- [`__thesis__`](./__thesis__/): Contains the LaTeX source code for my thesis, abstracts, etc.
- [`.githooks`](./.githooks/): Contains the source code for the git hooks, these provide linting and style checks upon committing code.
- [`.gitlab`](./.gitlab/): Contains Markdown templates for GitLab's issues and merge requests.
- [`.vscode`](./.vscode/): Contains configuration details for my personal code editor of choice: Visual Studio Code.
- [`agent`](./agent/): Contains the Python source code for the various RL agent implementations, and everything related to it.
- [`environment`](./environment/): Contains the Python source code for the environment wrappers.
- [`logs`](./logs/): This is where logs are outputted upon running `make run`, it also contains the data used as results in the thesis.
- [`tests`](./tests/): Contains the Jupyter Notebooks used for testing, experimenting, and plotting data for the various agents.

### Files

- [`.gitattributes`](./.gitattributes): Contains definitions and configuration for git and git LFS.
- [`.gitignore`](./.gitignore): Defines files that should not be stored on the remote git repository.
- [`main.py`](./main.py): The entry point for the program, should be run like so: `python main.py /path/to/log/dir/` or by running `make run` (Linux / Mac).
- [`Makefile`](./Makefile): Defines `make` commands that simplify development and execution of the program (getting this to work on Windows will be a little trickier).
- [`README.md`](./README.md): The file you're reading right now. ;)
- [`requirements.txt`](./requirements.txt): A text file containing all pip dependencies, used for setting up the Python virtual environment.
