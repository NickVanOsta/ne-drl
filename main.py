"""
Author: Nick Van Osta
"""


import os
import sys
import time
from typing import Callable, List, Tuple

import numpy as np
import pandas as pd
import seaborn as sns

from agent import Agent, EpisodeStopConditionOrLimit, Percept, RunStopConditionOrLimit
from agent.nedrl.neat import NEATDRLAgent
from agent.neuroevolution.neat import NEATAgent
from agent.q_learning.deep import DeepQLearningAgent
from agent.q_learning.tabular import TabularQLearningAgent
from agent.random import RandomAgent
from environment import AgentConfig
from environment.cart_pole import CartPoleEnv
from environment.cliff_walking import CliffWalkingEnv
from environment.frozen_lake import FrozenLakeEnv
from environment.mountain_car import MountainCarEnv

wrapper = CartPoleEnv()


def print_config(file: str):
    """Prints the contents of the given configuration file.

    Args:
        file (str): Path to the configuration file.
    """
    with open(file, mode="r", encoding="utf-8") as f:
        while l := f.readline():
            print(l.strip())


def learn(
    agent: Agent,
    run_stop_condition: RunStopConditionOrLimit,
    episode_stop_condition: EpisodeStopConditionOrLimit,
    should_render: bool = False,
) -> Tuple[float, List[float], List[List[Percept]]]:
    """Runs the training phase for the given agent.

    Args:
        agent (Agent): The agent to train.
        run_stop_condition (RunStopConditionOrLimit): A function defining when to stop the training phase or an episode limit.
        episode_stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
        should_render (bool, optional): Whether the environment should be rendered. Defaults to False.

    Returns:
        Tuple[float, List[float], List[List[Percept]]]: The total reward, reward history, and percept histories.
    """

    start = time.time()
    total_reward, reward_history, episode_history = agent.fit(
        run_stop_condition, episode_stop_condition, should_render, True
    )
    end = time.time()

    agent._save_model()

    n = len(reward_history)

    print("epochs        : {}".format(n))
    print("total reward  : {:.3f}".format(total_reward))
    print("average reward: {:.3f}".format(total_reward / n))
    print("duration (s)  : {:.3f}".format(end - start))
    print()

    return total_reward, reward_history, episode_history


def test(
    agent: Agent,
    run_stop_condition: RunStopConditionOrLimit,
    episode_stop_condition: EpisodeStopConditionOrLimit,
    should_render: bool = False,
) -> Tuple[float, List[float], List[List[Percept]]]:
    """Runs the testing phase for the given agent.

    Args:
        agent (Agent): The agent to test.
        run_stop_condition (RunStopConditionOrLimit): A function defining when to stop the training phase or an episode limit.
        episode_stop_condition (EpisodeStopConditionOrLimit): A function defining when to stop an episode or a step limit.
        should_render (bool, optional): Whether the environment should be rendered. Defaults to False.

    Returns:
        Tuple[float, List[float], List[List[Percept]]]: The total reward, reward history, and percept histories.
    """

    start = time.time()
    total_reward, reward_history, episode_history = agent.run(
        run_stop_condition, episode_stop_condition, should_render, False
    )
    end = time.time()

    n = len(reward_history)

    print("epochs        : {}".format(n))
    print("total reward  : {:.3f}".format(total_reward))
    print("average reward: {:.3f}".format(total_reward / n))
    print("duration (s)  : {:.3f}".format(end - start))
    print()

    return total_reward, reward_history, episode_history


# Uncomment to disable GPU
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def run_agent(
    agent: Agent,
    config: AgentConfig,
    render_learning=False,
    render_testing=True,
    continuous=False,
) -> Tuple[
    float, List[float], List[List[Percept]], float, List[float], List[List[Percept]]
]:
    """Run training and testing phases for a given agent.

    Args:
        agent (Agent): The agent to train and test.
        config (AgentConfig): The agent's configuration settings.
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.

    Returns:
        Tuple[ float, List[float], List[List[Percept]], float, List[float], List[List[Percept]] ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    l_total_reward, l_reward_history, l_episode_history = learn(
        agent,
        config.train_stop_condition,
        config.episode_stop_condition,
        render_learning,
    )

    if not continuous:
        input()

    t_total_reward, t_reward_history, t_episode_history = test(
        agent,
        config.test_stop_condition,
        config.episode_stop_condition,
        render_testing,
    )
    print()

    return (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def run_random_agent(
    render_learning=False, render_testing=True, continuous=False, cold_start=False
) -> Tuple[
    RandomAgent,
    float,
    List[float],
    List[List[Percept]],
    float,
    List[float],
    List[List[Percept]],
]:
    """Run the random agent.

    Args:
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.
        cold_start (bool, optional): Whether to initialize the agent with a new model, or load a previously saved one. Defaults to False.

    Returns:
        Tuple[ RandomAgent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]], ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    print("=== Random Agent ===")
    random = RandomAgent(wrapper.env)
    (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    ) = run_agent(
        random, wrapper.random_agent, render_learning, render_testing, continuous
    )

    return (
        random,
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def run_q_agent_tabular(
    render_learning=False, render_testing=True, continuous=False, cold_start=False
) -> TabularQLearningAgent:
    """Run the tabular Q-Learning agent.

    Args:
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.
        cold_start (bool, optional): Whether to initialize the agent with a new model, or load a previously saved one. Defaults to False.

    Returns:
        Tuple[ RandomAgent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]], ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    print("=== Tabular Q-Learning Agent ===")
    ql = TabularQLearningAgent(
        wrapper.env,
        wrapper.tabular_q_agent.exploration_rate_min,
        wrapper.tabular_q_agent.exploration_rate_max,
        wrapper.tabular_q_agent.exploration_rate_decay,
        wrapper.tabular_q_agent.decay_frequency,
        wrapper.learning_rate,
        wrapper.discount,
        wrapper.save_frequency,
        wrapper.tabular_q_agent.float_min,
        wrapper.tabular_q_agent.float_max,
        wrapper.tabular_q_agent.float_samples,
        "__data__/ql/{}".format(wrapper.save_file),
        wrapper.process_percept,
        cold_start,
    )
    (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    ) = run_agent(
        ql, wrapper.tabular_q_agent, render_learning, render_testing, continuous
    )

    return (
        ql,
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def run_neat_agent(
    render_learning=False, render_testing=True, continuous=False, cold_start=False
):
    """Run the NEAT agent.

    Args:
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.
        cold_start (bool, optional): Whether to initialize the agent with a new model, or load a previously saved one. Defaults to False.

    Returns:
        Tuple[ RandomAgent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]], ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    print("=== NEAT Agent ===")
    neat = NEATAgent(
        wrapper.env,
        wrapper.neat_agent.exploration_rate_min,
        wrapper.neat_agent.exploration_rate_max,
        wrapper.neat_agent.exploration_rate_decay,
        wrapper.neat_agent.decay_frequency,
        wrapper.learning_rate,
        wrapper.discount,
        wrapper.config_file,
        wrapper.neat_agent.generations_per_episode,
        wrapper.save_frequency,
        "__data__/neat/{}".format(wrapper.save_file),
        wrapper.process_percept,
        cold_start,
    )
    (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    ) = run_agent(neat, wrapper.neat_agent, render_learning, render_testing, continuous)

    return (
        neat,
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def run_q_agent_deep(
    render_learning=False, render_testing=True, continuous=False, cold_start=False
):
    """Run the Deep Q-Learning agent.

    Args:
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.
        cold_start (bool, optional): Whether to initialize the agent with a new model, or load a previously saved one. Defaults to False.

    Returns:
        Tuple[ RandomAgent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]], ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    print("=== Deep Q-Learning Agent ===")
    dql = DeepQLearningAgent(
        wrapper.env,
        wrapper.deep_q_agent.exploration_rate_min,
        wrapper.deep_q_agent.exploration_rate_max,
        wrapper.deep_q_agent.exploration_rate_decay,
        wrapper.deep_q_agent.decay_frequency,
        wrapper.learning_rate,
        wrapper.discount,
        wrapper.save_frequency,
        wrapper.deep_q_agent.update_target_frequency,
        wrapper.deep_q_agent.batch_size,
        wrapper.deep_q_agent.memory_limit,
        wrapper.model_builder,
        wrapper.model_predictor,
        wrapper.model_fitter,
        wrapper.model_copier,
        wrapper.model_loader,
        wrapper.model_saver,
        "__data__/dql/{}".format(wrapper.save_file),
        wrapper.process_percept,
        cold_start,
    )
    (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    ) = run_agent(
        dql, wrapper.deep_q_agent, render_learning, render_testing, continuous
    )

    return (
        dql,
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def run_neat_drl_agent(
    render_learning=False, render_testing=True, continuous=False, cold_start=False
):
    """Run the NEAT-DRL agent.

    Args:
        render_learning (bool, optional): Whether to render the environment during the training phase. Defaults to False.
        render_testing (bool, optional): Whether to render the environment during the testing phase. Defaults to True.
        continuous (bool, optional): Whether to run the testing phase immediately after the training phase, or wait for user input to continue. Defaults to False.
        cold_start (bool, optional): Whether to initialize the agent with a new model, or load a previously saved one. Defaults to False.

    Returns:
        Tuple[ RandomAgent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]], ]: The total reward, reward history, and percept histories for both the training and testing phase.
    """

    print("=== NEAT DRL Agent ===")
    neat_drl = NEATDRLAgent(
        wrapper.env,
        wrapper.neat_drl_agent.exploration_rate_min,
        wrapper.neat_drl_agent.exploration_rate_max,
        wrapper.neat_drl_agent.exploration_rate_decay,
        wrapper.neat_drl_agent.decay_frequency,
        wrapper.learning_rate,
        wrapper.discount,
        wrapper.save_frequency,
        wrapper.neat_drl_agent.update_target_frequency,
        wrapper.neat_drl_agent.batch_size,
        wrapper.neat_drl_agent.memory_limit,
        wrapper.config_file,
        wrapper.neat_drl_agent.generations_per_update,
        "__data__/neat-drl/{}".format(wrapper.save_file),
        wrapper.process_percept,
        cold_start,
    )
    (
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    ) = run_agent(
        neat_drl, wrapper.neat_drl_agent, render_learning, render_testing, continuous
    )

    return (
        neat_drl,
        l_total_reward,
        l_reward_history,
        l_episode_history,
        t_total_reward,
        t_reward_history,
        t_episode_history,
    )


def plot_rewards(reward_history: List[float], loc: str, label: str):
    """Generate plots for the given reward history and save it to a file.

    Args:
        reward_history (List[float]): A chronological list of earned rewards over a training/testing phase.
        loc (str): Directory to save the plot to.
        label (str): Label used for generating the filename.
    """

    p: sns.FacetGrid = sns.relplot(
        x=[x for x in range(0, len(reward_history))], y=reward_history, kind="line"
    )
    p.set(xlabel="Episode", ylabel="Reward")

    p.savefig(
        "{}/{}-reward.png".format(loc, label),
        format="png",
    )


def plot_fitness(fitness_history: List[float], loc: str, label: str):
    """Generate plots for the given fitness score history and save it to a file.

    Args:
        fitness_history (List[float]): A chronological list of top fitness scores over an NE's training phase.
        loc (str): Directory to save the plot to.
        label (str): Label used for generating the filename.
    """

    p: sns.FacetGrid = sns.relplot(
        x=[x for x in range(0, len(fitness_history))], y=fitness_history, kind="line"
    )
    p.set(xlabel="Generation", ylabel="Fitness score")

    p.savefig(
        "{}/{}-fitness.png".format(loc, label),
        format="png",
    )


def plot_agent(
    agent: str,
    loc: str,
    l_reward_history: List[float],
    l_episode_history: List[List[Percept]],
    t_reward_history: List[float],
    t_episode_history: List[List[Percept]],
):
    """Generate plots for the given agent.

    Args:
        agent (str): Textual name or description of the agent.
        loc (str): Directory to save the plots to.
        l_reward_history (List[float]): A chronological list of earned rewards over a training phase.
        l_episode_history (List[List[Percept]]): A chronological list of episodes over a training phase.
        t_reward_history (List[float]): A chronological list of earned rewards over a testing phase.
        t_episode_history (List[List[Percept]]): A chronological list of episodes over a testing phase.
    """

    plot_rewards(l_reward_history, loc, "{}-{}".format(agent, "L"))
    plot_rewards(t_reward_history, loc, "{}-{}".format(agent, "T"))


def remove_outliers(data: np.ndarray, factor=3.0) -> np.ndarray:
    """Creates a new dataset with outliers removed.

    Args:
        data (np.ndarray): Original dataset.
        factor (float, optional): IQR factor that determines the border values for outliers. Defaults to 3.0.

    Returns:
        np.ndarray: The new dataset, with outliers removed.
    """

    q1, q3 = np.percentile(data, [25, 75])
    iqr = q3 - q1
    lower, upper = q1 - factor * iqr, q3 + factor * iqr

    return np.array(data)[(data <= upper) & (data >= lower)]


RunAgentFunc = Callable[
    [bool, bool, bool, bool],
    Tuple[
        Agent,
        float,
        List[float],
        List[List[Percept]],
        float,
        List[float],
        List[List[Percept]],
    ],
]
"""A function type definition that represents the different 'run_*_agent' functions."""


def test_agent(
    name: str, run_agent_fn: RunAgentFunc
) -> Tuple[
    Agent,
    float,
    List[float],
    List[List[Percept]],
    float,
    List[float],
    List[List[Percept]],
]:

    """Wrapper for running and testing any agent.

    Args:
        name (str): Textual name or description of the agent.
        run_agent_fn (RunAgentFunc): Function defining the agent's run functionality.

    Returns:
        Tuple[ Agent, float, List[float], List[List[Percept]], float, List[float], List[List[Percept]] ]: The agent, total reward, reward history, and episode history for both training and testing phase.
    """

    cold_start = False
    render_learning = False
    render_testing = False
    continuous = True

    (
        agents,
        total_l_total_reward,
        total_l_reward_history,
        total_l_episode_history,
        total_t_total_reward,
        total_t_reward_history,
        total_t_episode_history,
    ) = (
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    )

    N = 10
    for n in range(N):
        (
            agent,
            agent_l_total_reward,
            agent_l_reward_history,
            agent_l_episode_history,
            agent_t_total_reward,
            agent_t_reward_history,
            agent_t_episode_history,
        ) = run_agent_fn(render_learning, render_testing, continuous, cold_start)
        plot_agent(
            "{}-{}".format(name, n),
            sys.argv[1],
            agent_l_reward_history,
            agent_l_episode_history,
            agent_t_reward_history,
            agent_t_episode_history,
        )

        pd.DataFrame(agent_l_reward_history).to_csv(
            "{}/{}-{}-L-rewards.csv".format(sys.argv[1], name, n)
        )
        pd.DataFrame(agent_l_episode_history).to_csv(
            "{}/{}-{}-L-episodes.csv".format(sys.argv[1], name, n)
        )
        pd.DataFrame(agent_t_reward_history).to_csv(
            "{}/{}-{}-T-rewards.csv".format(sys.argv[1], name, n)
        )
        pd.DataFrame(agent_t_episode_history).to_csv(
            "{}/{}-{}-T-episodes.csv".format(sys.argv[1], name, n)
        )

        agents.append(agent)
        total_l_total_reward.append(agent_l_total_reward)
        total_l_reward_history.append(agent_l_reward_history)
        total_l_episode_history.append(agent_l_episode_history)
        total_t_total_reward.append(agent_t_total_reward)
        total_t_reward_history.append(agent_t_reward_history)
        total_t_episode_history.append(agent_t_episode_history)

    pd.DataFrame(total_l_total_reward).to_csv(
        "{}/{}-L-total-rewards.csv".format(sys.argv[1], name)
    )
    pd.DataFrame(total_l_reward_history).to_csv(
        "{}/{}-L-rewards.csv".format(sys.argv[1], name)
    )
    pd.DataFrame(total_l_episode_history).to_csv(
        "{}/{}-L-episodes.csv".format(sys.argv[1], name)
    )
    pd.DataFrame(total_t_total_reward).to_csv(
        "{}/{}-T-total-rewards.csv".format(sys.argv[1], name)
    )
    pd.DataFrame(total_t_reward_history).to_csv(
        "{}/{}-T-rewards.csv".format(sys.argv[1], name)
    )
    pd.DataFrame(total_t_episode_history).to_csv(
        "{}/{}-T-episodes.csv".format(sys.argv[1], name)
    )

    return (
        agents,
        total_l_total_reward,
        total_l_reward_history,
        total_l_episode_history,
        total_t_total_reward,
        total_t_reward_history,
        total_t_episode_history,
    )


if __name__ == "__main__":
    print_config(wrapper.config_file)

    (
        randoms,
        random_l_total_reward,
        random_l_reward_history,
        random_l_episode_history,
        random_t_total_reward,
        random_t_reward_history,
        random_t_episode_history,
    ) = test_agent("RANDOM", run_random_agent)

    (
        neats,
        neat_l_total_reward,
        neat_l_reward_history,
        neat_l_episode_history,
        neat_t_total_reward,
        neat_t_reward_history,
        neat_t_episode_history,
    ) = test_agent("NEAT", run_neat_agent)

    (
        dqls,
        dql_l_total_reward,
        dql_l_reward_history,
        dql_l_episode_history,
        dql_t_total_reward,
        dql_t_reward_history,
        dql_t_episode_history,
    ) = test_agent("DQL", run_q_agent_deep)

    (
        neat_drls,
        neat_drl_l_total_reward,
        neat_drl_l_reward_history,
        neat_drl_l_episode_history,
        neat_drl_t_total_reward,
        neat_drl_t_reward_history,
        neat_drl_t_episode_history,
    ) = test_agent("NEAT-DRL", run_neat_drl_agent)
