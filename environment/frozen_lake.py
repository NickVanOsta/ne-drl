"""
Author: Nick Van Osta
"""


from typing import List

import gym
import numpy as np
import tensorflow as tf
from agent import Action, LossFunction, Percept, State
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.losses import Huber
from tensorflow.keras.optimizers import Adam

from environment import EnvWrapper


class FrozenLakeEnv(EnvWrapper):
    """Represents a wrapper object for the FrozenLake environment."""

    def __init__(self) -> None:
        super().__init__(
            gym.make("FrozenLake-v1", render_mode="human", new_step_api=False),
            "__data__/.config/frozen_lake.ini",
            self.__builder,
            self.__predictor,
            self.__fitter,
            self.__copier,
            lambda savefile: keras.models.load_model(savefile),
            lambda m, save_file: m.save(save_file),
            self.__process_percept,
        )

    def __builder(self, n_states: int, n_actions: int) -> keras.Model:
        """The model builder function definition.

        Args:
            n_states (int): The amount of states (inputs).
            n_actions (int): The amount of actions (outputs).

        Returns:
            keras.Model: The built Keras model.
        """

        state_shape = (*n_states,)
        action_shape = np.product(n_actions)

        inputs = layers.Input(shape=state_shape)

        hidden = layers.Dense(10, input_dim=state_shape, activation="elu")(inputs)
        action = layers.Dense(action_shape, activation="linear")(hidden)

        optimizer = Adam(learning_rate=self._learning_rate)
        loss = Huber()
        model = keras.Model(inputs=inputs, outputs=action)
        model.compile(optimizer, loss)

        return model

    def __predictor(self, model: keras.Model, s: State) -> List[Action]:
        """The model predictor function definition.

        Args:
            model (keras.Model): The Keras model used to perform the predictions.
            s (State): The input state(s) to predict from.

        Returns:
            List[Action]: The predicted actions.
        """

        inputs = tf.convert_to_tensor(s)

        if len(inputs.shape) < 2:
            inputs = tf.expand_dims(inputs, 0)

        return model.predict(inputs)

    def __fitter(
        self,
        model: keras.Model,
        S: List[State],
        Y: List[Action],
        loss: LossFunction,
    ) -> keras.Model:
        """The model fitter function definition.

        Args:
            model (keras.Model): The Keras model to fit.
            S (List[State]): The list of input states.
            Y (List[Action]): The list of expected actions.
            loss (LossFunction): The used loss function.

        Returns:
            keras.Model: The fitted Keras model.
        """

        model.fit(S, Y, epochs=self.deep_q_agent.epochs_per_update, verbose=0)
        return model

    def __copier(self, target: keras.Model, model: keras.Model) -> keras.Model:
        """The model copier function definition.

        Args:
            target (keras.Model): The copy target.
            model (keras.Model): The copy source.

        Returns:
            keras.Model: The updated copy target.
        """

        target.set_weights(model.get_weights())
        return target

    def __process_percept(self, p: Percept) -> Percept:
        """Processes the given percept and updates the reward.

        Args:
            p (Percept): The original percept.

        Returns:
            Percept: The processed percept.
        """

        r = (p.r * 2 - 1) * p.is_terminal

        return Percept(p.s, p.s_new, p.a, r, p.is_terminal, p.info)
