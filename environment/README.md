# Neuro-Evolutionary Deep Reinforcement Learning

## `environment`

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

This folder contains the Python 3 code for the `environment` module.
This module handles everything to do with the RL environments and their configurations, including agent configurations per environment.

## Citing

When citing anything from the source code, please cite it as follows:

```bibtex
@mastersthesis{vanosta:2022,
  author = {Van Osta, Nick},
  title  = {Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control},
  school = {University Ghent},
  year   = {2022}
}
```

## Outline

### Files

- [`__init__.py`](./__init__.py): Contains the code for the `environment` module, defines environment wrappers containing the agents' configuration.
- [`cart_pole.py`](./cart_pole.py): Contains the environment wrapper and configuration for the CartPole-v1 environment.
- [`cliff_walking.py`](./cliff_walking.py): Contains the environment wrapper and configuration for the CliffWalking-v0 environment.
- [`frozen_lake.py`](./frozen_lake.py): Contains the environment wrapper and configuration for the FrozenLake-v1 environment.
- [`mountain_car.py`](./mountain_car.py): Contains the environment wrapper and configuration for the MountainCar-v1 environment.
