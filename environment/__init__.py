"""
Author: Nick Van Osta
"""


from configparser import ConfigParser

from agent import (
    ModelBuilder,
    ModelCopier,
    ModelFitter,
    ModelLoader,
    ModelPredictor,
    ModelSaver,
    ProcessPercept,
    RunStopConditionOrLimit,
    EpisodeStopConditionOrLimit,
)
from gym import Env


class AgentConfig(object):
    """Represents the general configuration for an RL agent."""

    def __init__(self, config: ConfigParser, section: str) -> None:
        self._epsilon_max = float(config[section]["EpsilonMax"])
        self._epsilon_min = float(config[section]["EpsilonMin"])
        self._epsilon_decay = float(config[section]["EpsilonDecay"])
        self._decay_frequency = int(config[section]["DecayFrequency"])

        self._max_steps = int(config[section]["MaxSteps"])
        self._episode_count = int(config[section]["EpisodeCount"])
        self._test_count = int(config[section]["TestCount"])

    @property
    def exploration_rate_max(self) -> float:
        """The maximum exploration rate."""
        return self._epsilon_max

    @property
    def exploration_rate_min(self) -> float:
        """The minimum exploration rate."""
        return self._epsilon_min

    @property
    def exploration_rate_decay(self) -> float:
        """The rate at which the exploration rate decays."""
        return self._epsilon_decay

    @property
    def decay_frequency(self) -> int:
        """The frequency at which the exploration rate decays."""
        return self._decay_frequency

    @property
    def episode_stop_condition(self) -> EpisodeStopConditionOrLimit:
        """The function defining when to stop an episode or a step limit."""
        return self._max_steps

    @property
    def train_stop_condition(self) -> RunStopConditionOrLimit:
        """The function defining when to stop the training phase or an episode limit."""
        return self._episode_count

    @property
    def test_stop_condition(self) -> RunStopConditionOrLimit:
        """The function defining when to stop the testing phase or an episode limit."""
        return self._test_count


class RandomAgentConfig(AgentConfig):
    """Represents the configuration for a Random Agent."""

    SECTION = "RANDOM-AGENT"

    def __init__(self, config: ConfigParser) -> None:
        super().__init__(config, self.SECTION)


class TabularQAgentConfig(AgentConfig):
    """Represents the configuration for a Tabular Q-Learning Agent."""

    SECTION = "Q-AGENT-TABULAR"

    def __init__(self, config: ConfigParser) -> None:
        super().__init__(config, self.SECTION)

        self._float_samples = int(config[self.SECTION]["FloatSamples"])
        self._float_min = float(config[self.SECTION]["FloatMin"])
        self._float_max = float(config[self.SECTION]["FloatMax"])

    @property
    def float_samples(self) -> int:
        """The amount of samples to take when dealing with continuous state space."""
        return self._float_samples

    @property
    def float_min(self) -> float:
        """The minimum value to consider when dealing with continuous state space."""
        return self._float_min

    @property
    def float_max(self) -> float:
        """The maximum value to consider when dealing with continuous state space."""
        return self._float_max


class DeepQAgentConfig(AgentConfig):
    """Represents the configuration for a Deep Q-Learning Agent."""

    SECTION = "Q-AGENT-DEEP"

    def __init__(self, config: ConfigParser) -> None:
        super().__init__(config, self.SECTION)

        self._update_target_frequency = int(
            config[self.SECTION]["UpdateTargetFrequency"]
        )
        self._batch_size = int(config[self.SECTION]["BatchSize"])
        self._memory_limit = int(config[self.SECTION]["MemoryLimit"])
        self._epochs_per_update = int(config[self.SECTION]["EpochsPerUpdate"])

    @property
    def update_target_frequency(self) -> int:
        """The amount of episodes between each target model update."""
        return self._update_target_frequency

    @property
    def batch_size(self) -> int:
        """The mini-batch size for the training of the model."""
        return self._batch_size

    @property
    def memory_limit(self) -> int:
        """The maximum amount of percepts to store in the replay buffer."""
        return self._memory_limit

    @property
    def epochs_per_update(self) -> int:
        """The amount of fitting epochs to run per model update."""
        return self._epochs_per_update


class NEATAgentConfig(AgentConfig):
    """Represents the configuration for a NEAT Agent."""

    SECTION = "NEAT-AGENT"

    def __init__(self, config: ConfigParser) -> None:
        super().__init__(config, self.SECTION)

        self._generations_per_episode = int(
            config[self.SECTION]["GenerationsPerEpisode"]
        )

    @property
    def generations_per_episode(self) -> int:
        """The amount of generations to run per episode."""
        return self._generations_per_episode


class NEATDRLAgentConfig(AgentConfig):
    """Represents the configuration for a NEAT-DRL Agent."""

    SECTION = "NEAT-DRL-AGENT"

    def __init__(self, config: ConfigParser) -> None:
        super().__init__(config, self.SECTION)

        self._update_target_frequency = int(
            config[self.SECTION]["UpdateTargetFrequency"]
        )
        self._batch_size = int(config[self.SECTION]["BatchSize"])
        self._memory_limit = int(config[self.SECTION]["MemoryLimit"])
        self._generations_per_update = int(config[self.SECTION]["GenerationsPerUpdate"])

    @property
    def update_target_frequency(self) -> int:
        """The amount of episodes between each target model update."""
        return self._update_target_frequency

    @property
    def batch_size(self) -> int:
        """The mini-batch size for the training of the model."""
        return self._batch_size

    @property
    def memory_limit(self) -> int:
        """The maximum amount of percepts to store in the replay buffer."""
        return self._memory_limit

    @property
    def generations_per_update(self) -> int:
        """The amount of generations to run per model update."""
        return self._generations_per_update


class EnvWrapper(object):
    """Represents a wrapper object for an environment, which includes the agents' configurations."""

    def __init__(
        self,
        env: Env,
        config_file: str,
        model_builder: ModelBuilder = lambda save_file: None,
        model_predictor: ModelPredictor = lambda model, s: None,
        model_fitter: ModelFitter = lambda model, s, a: None,
        model_copier: ModelCopier = lambda target, model: target,
        model_loader: ModelLoader = lambda save_file: None,
        model_saver: ModelSaver = lambda model, save_file: None,
        process_percept: ProcessPercept = lambda p: p,
    ) -> None:
        self._config_file = config_file
        self._env = env
        self._process_percept = process_percept

        config = ConfigParser()
        config.read(config_file)

        self._save_frequency = int(config["GENERAL"]["SaveFrequency"])
        self._save_file = config["GENERAL"]["SaveFile"]
        self._learning_rate = float(config["GENERAL"]["LearningRate"])
        self._discount_rate = float(config["GENERAL"]["DiscountRate"])

        self._random_agent_config = RandomAgentConfig(config=config)
        self._q_agent_tabular_config = TabularQAgentConfig(config=config)
        self._q_agent_deep_config = DeepQAgentConfig(config=config)
        self._neat_agent_config = NEATAgentConfig(config=config)
        self._neat_drl_agent_config = NEATDRLAgentConfig(config=config)

        self._model_builder = model_builder
        self._model_predictor = model_predictor
        self._model_fitter = model_fitter
        self._model_copier = model_copier

        self._model_loader = model_loader
        self._model_saver = model_saver

    @property
    def random_agent(self) -> RandomAgentConfig:
        """The random agent's configuration."""
        return self._random_agent_config

    @property
    def tabular_q_agent(self) -> TabularQAgentConfig:
        """The Tabular Q-Learning agent's configuration."""
        return self._q_agent_tabular_config

    @property
    def deep_q_agent(self) -> DeepQAgentConfig:
        """The Deep Q-Learning agent's configuration."""
        return self._q_agent_deep_config

    @property
    def neat_agent(self) -> NEATAgentConfig:
        """The NEAT agent's configuration."""
        return self._neat_agent_config

    @property
    def neat_drl_agent(self) -> NEATDRLAgentConfig:
        """The NEAT-DRL agent's configuration."""
        return self._neat_drl_agent_config

    @property
    def config_file(self) -> str:
        """The file path of the configuration file."""
        return self._config_file

    @property
    def env(self) -> Env:
        """The wrapped environment."""
        return self._env

    @property
    def process_percept(self) -> ProcessPercept:
        """A percept processing function."""
        return self._process_percept

    @property
    def save_frequency(self) -> int:
        """The amount of episodes between model saves."""
        return self._save_frequency

    @property
    def save_file(self) -> str:
        """The file location for the saved model."""
        return self._save_file

    @property
    def learning_rate(self) -> float:
        """The agent's learning rate."""
        return self._learning_rate

    @property
    def discount(self) -> float:
        """The applied discount factor."""
        return self._discount_rate

    @property
    def model_builder(self) -> ModelBuilder:
        """The model builder function definition."""
        return self._model_builder

    @property
    def model_predictor(self) -> ModelPredictor:
        """The model predictor function definition."""
        return self._model_predictor

    @property
    def model_fitter(self) -> ModelFitter:
        """The model fitter function definition."""
        return self._model_fitter

    @property
    def model_copier(self) -> ModelCopier:
        """The model copier function definition."""
        return self._model_copier

    @property
    def model_loader(self) -> ModelLoader:
        """The model loader function definition."""
        return self._model_loader

    @property
    def model_saver(self) -> ModelSaver:
        """The model saver function definition."""
        return self._model_saver

    def fitness(genomes, config):
        """The general fitness function of standard NE agents."""

        raise NotImplementedError()
