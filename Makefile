#!make

.PHONY: init hooks install run

datetime := $(shell date +"%Y%m%d%H%M%S")

init: hooks init-venv ## Initialize repository

hooks: ## Initialize the project's git hooks
	@git config core.hooksPath .githooks

init-venv: ## Initialize the project's virtual environment
	@python3 -m venv .venv

install: ## Install dependencies
	@pip install -r requirements.txt

run: ## Run the main file
	@mkdir -p "logs/$(datetime)/"
	@python main.py "logs/$(datetime)/" | tee "logs/$(datetime)/output.log"

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
