\documentclass[conference,a4paper]{IEEEtran}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts,dsfont}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage[acronym, toc]{glossaries}
\usepackage[labelformat=simple]{subcaption}
\usepackage[hyphens]{url}
\usepackage[colorlinks = true,
breaklinks=true,
linkcolor=black,
anchorcolor=black,
citecolor=black,
filecolor=black,
urlcolor=blue]{hyperref}
\usepackage{balance}

\graphicspath{{images/}}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Custom maths commands
\newcommand{\argmax}{
    \mathop{\mathrm{argmax}}\limits
}
\newcommand{\argmin}[1]{
    \mathop{\mathrm{argmin}}\limits
}

\bibliographystyle{IEEEtran}
\input{chapters/glossary-nl}

\begin{document}
\title{Neuro-Evolutioneel Deep Reinforcement Learning voor Robotische Controle}

\author{\Large{Nick Van Osta}\\
    \\
    \large{Promotor: Prof. dr. ir. Aleksandra Pižurica}\\
    \large{Begeleider: Srđan Lazendić}\\
    \\
    Masterproef ingediend tot het behalen van de academische graad van\\
    Master of Science in de industriële wetenschappen: informatica\\
    Academiejaar 2021-2022
}

\maketitle

\begin{abstract}
    De opkomst van \acrfull{drl} heeft geleid tot veel verbeteringen op het gebied van \acrfull{ai}.
    De introductie van \acrfullpl{ann} en \acrfullpl{dnn} bracht de mogelijkheid om complexere omgevingen af te handelen
    en verbeterde \acrfull{rl} technieken in het algemeen.
    Echter, de toevoeging van \acrshortpl{dnn} kwam met de extra
    moeilijkheid om een geschikte netwerkarchitectuur te ontwerpen
    voor het specifieke \acrshort{rl} probleem.
    Om deze last van de gebruiker te verlichten, stellen wij het gebruik van \acrfull{ne} voor
    om de netwerkarchitectuur voor de gewenste omgeving automatisch te laten genereren.
    We hebben dit bereikt door het uitwisselen van de
    traditionele \acrshort{dnn} in het \acrfull{dql} algoritme
    met een model gegenereerd door het \acrfull{neat} algoritme.
    Bovendien introduceren we technieken om het leerproces voor het voorgestelde
    algoritme te stabiliseren.
    Hoewel de verkregen resultaten niet kunnen opboksen tegen de state-of-the-art \acrshort{drl} methoden,
    zal deze nieuwe aanpak meer inzicht geven in de mogelijkheid om de technieken van
    \acrshort{drl} en \acrshort{ne} te combineren.
\end{abstract}

\begin{IEEEkeywords}
    Reinforcement Learning, Deep Learning, Genetische Algoritmes, Neuro-Evolutie
\end{IEEEkeywords}

\section{Introductie}

\Gls{drl} is een snel evoluerend onderzoekspad op het gebied van machinaal leren,
vaak aangeduid als de technologie die naar verwachting een revolutie teweeg zal brengen op het gebied van \gls{ai}.
De opkomst van \gls{dl} leverde nieuwe hulpmiddelen om deze problemen op te lossen,
wat resulteerde in een krachtig nieuw raamwerk: \Gls{drl}.
\Gls{drl} heeft al opmerkelijke resultaten geboekt in verschillende toepassingen,
variërend van het spelen van videospelletjes~\cite{verleysen:2019}
tot binnenhuisnavigatie en met verschillende \glspl{agent} waaronder robots en soft bots~\cite{verleysen:2019, haarnoja:2018, nguyen:2019}.
Besturingsbeleid voor robots kan bijvoorbeeld direct worden geleerd van camera-invoer in de echte wereld~\cite{verleysen:2019}.

Een ander onderzoekspad dat de laatste tijd sterk in de belangstelling staat, is dat van de \gls{ne}.
\Gls{ne} is een evolutionaire methode om \glspl{ann} te genereren
die zichzelf bewezen heeft in het oplossen van een breed scala aan problemen en taken,
waaronder \glspl{rl} problemen.
Door de jaren heen is er een verscheidenheid aan algoritmen getheoretiseerd en gepubliceerd,
de meest prominente zijnde: \gls{neat} (2002)~\cite{stanley:2002}.
Hoewel dit algoritme gedateerd lijkt, biedt het een robuust raamwerk dat jaren na haar publicatie nog steeds toepasbaar is.
\Gls{ne} is in de loop der jaren op verschillende problemen toegepast, voorbeelden zijn:
een grote verscheidenheid aan Atari spellen~\cite{salimans:2017};
het populaire Nintendo-spel, Super Mario World~\cite{sethbling:2015};
en robotische, meerbenige voortbeweging~\cite{valsalam:2008}.
In sommige gevallen is zelfs bewezen dat \gls{ne} beter presteert dan
\gls{rl} algoritmen~\cite{salimans:2017, such:2017}.

\Gls{drl} kan een krachtig gereedschap zijn om verschillende problemen op te lossen,
maar het heeft ook enkele nadelen.
Het grootste nadeel bij de toepassing van \gls{drl} is het bepalen van de netwerktopologie voor het gebruikte \gls{ann}.
De topologie van het netwerk, en vele andere hyperparameters die in de oplossingen van \gls{dl} voorkomen,
vereisen een aanzienlijke hoeveelheid afstelling, die momenteel aan de gebruiker wordt overgelaten.
Ons doel is om de last voor de gebruiker te verlichten en de afstelling van de \gls{ann} door de \gls{agent} te laten uitvoeren.
Om dit te doen, zullen we gebruik maken van de voordelen die \gls{ne} biedt om het netwerkmodel te genereren.
Het doel van dit proefschrift is niet om een aanpak te ontwikkelen die beter presteert dan de gevestigde methoden,
maar veeleer nieuwe inzichten te verschaffen in de mogelijkheden van het combineren van \gls{drl} en \gls{ne}.

Dit artikel zal vanaf nu op de volgende manier gestructureerd worden:
\begin{itemize}
    \item \emph{II. Voorbereidingen:} In dit hoofdstuk zullen de basis bouwstenen, namelijk \gls{dql} en \gls{neat},
          worden uitgelegd;
    \item \emph{III. Probleemstelling:} We zullen de problemen met de huidige benaderingen definiëren,
          en een basisuitleg geven van onze voorgestelde methode;
    \item \emph{IV. Voorgestelde methodes:} Hier wordt het voorgestelde raamwerk in detail toegelicht,
          en andere bijdragen om het algoritme te stabiliseren;
    \item \emph{V. Experimentele resultaten:} De experimentele resultaten en evaluaties worden in dit hoofdstuk besproken,
          dit behandelt zowel de resultaten met betrekking tot \gls{neat} als functie benaderer,
          als de resultaten voor onze \gls{nedrl} \gls{agent};
    \item en \emph{VI. Conclusie:} In dit laatste hoofdstuk worden enkele slotopmerkingen gegeven over het onderzoeksonderwerp.
\end{itemize}

\section{Voorbereidingen}

\subsection{Deep Q-learning}

\Gls{dql} is de \gls{drl} tegenhanger van het traditionele \gls{ql} algoritme.
In plaats van een benadering in tabelvorm te gebruiken voor de geschatte \gls{state}-waardefunctie $Q \left( s, a \right)$,
wordt een \gls{dnn} gebruikt als een niet-lineaire functie-benadering
voor de geschatte \gls{state}-waarde functie $Q \left( s, a ; \theta \right)$.
Het trainen van een \gls{dnn} is echter een leerprobleem onder supervisie.

Bij gesuperviseerd leren wordt verondersteld dat vooraf een volledige dataset beschikbaar is om op te trainen.
Dit is niet het geval bij \gls{drl}, waar de dataset wordt opgebouwd tijdens het trainen van de \gls{agent}.
Voor regulier gesuperviseerd leren geldt de \emph{stationariteit van doelen}, omdat de volledige dataset van tevoren wordt verkregen.
Voor \gls{drl} is dit echter niet het geval, omdat het doel verandert telkens wanneer nieuwe gegevens aan de dataset worden toegevoegd.

Bovendien wordt bij gesuperviseerd leren de dataset geschud voordat hij als trainingsgegevens aan het \gls{dnn} wordt verstrekt,
dit wordt \emph{batching} genoemd.
In \gls{drl} zijn de verzamelde gegevens temporeel gerelateerd,
een ervaringsmonster dat in tijdstap $t+1$ is verzameld, correleert met het ervaringsmonster dat in tijdstap $t$ is verzameld.
Hierdoor wordt het rangschikken van gegevens voor \gls{drl} een moeilijke zaak.

Een manier om het probleem van de stationariteit van de doelen te omzeilen is het gebruik van een doelnetwerk.
Hierdoor wordt het verplaatsen van het doel een paar episodes uitgesteld, zodat de \gls{dnn} tijd krijgt om te stabiliseren.
De formule voor het bijwerken van de gradiënt kan worden aangepast om het doelnetwerk met gewichten $theta^-$ te gebruiken.

Een manier om het probleem met batching data op te lossen, is het uitvoeren van \emph{ervaring replay}.
Deze techniek vermindert ook het probleem van de stationariteit van de doelen doordat
het netwerk meer gegevens heeft om op te trainen voordat het doel weer beweegt.

\subsection{NEAT}

Door \glspl{ga} te combineren met \glspl{ann} ontstaat wat men noemt \gls{ne}.
\Gls{ne} is een evolutionaire methode om \glspl{ann} te trainen of zelfs te construeren a.d.h.v. een \gls{ga}.
De moeilijkheid hier is om het \gls{ann} correct te coderen in een juist \gls{genotype},
om er zeker van te zijn dat de genetische operatoren zonder problemen kunnen worden uitgevoerd.
Sommige implementaties zijn zelfs in staat om \glspl{dnn} te construeren,
andere muteren enkel de parameters van het netwerk.

Een populaire en indrukwekkend goed presterende implementatie van \gls{ne} is \gls{neat}~\cite{stanley:2002}.
Het is in staat om recurrente, schaarse \glspl{ann} te maken via een uitgebreid \gls{ga}.
Dit gebeurt door middel van \gls{mutation}, \gls{crossover}, en \emph{speciatie},
waarbij gebruik wordt gemaakt van historische markeringen.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{NEAT-genotype.png}
    \caption{De codering van het \acrshort{neat} \gls{genotype}.}
    \label{fig:neat-genotype}
\end{figure}
In \gls{neat} worden de netwerken gecodeerd door een lijst bij te houden van \glspl{neuron} en \glspl{connection},
beide worden beschouwd als genen binnen het genotype.
Om zeker te zijn dat verschillende individuen naar hetzelfde \gls{neuron} of \gls{connection} verwijzen,
worden innovatienummers geïntroduceerd.
Innovatie is niet meer dan de toevoeging van een identificatiecode om bij te houden wanneer een gen
(hetzij een \gls{neuron}, hetzij een \gls{connection}) in de \gls{population} is geïntroduceerd,
met andere woorden, een globaal toenemend geheel getal.
Een voorbeeld van een koppeling tussen een \gls{phenotype} en een \gls{genotype} wordt getoond in Figuur~\ref{fig:neat-genotype},
waar zowel \gls{genotype} als \gls{phenotype} van hetzelfde netwerk naast elkaar zijn weergegeven.

De innovatienummers maken het mogelijk om aan \gls{crossover} tussen \glspl{genotype} te doen.
Genen met hetzelfde innovatienummer hebben dezelfde voorouderlijke oorsprong, en moeten dus bij elkaar passen.
De \gls{crossover} operator in \gls{neat} wordt het meest toegepast op de \gls{connection} genen.
Die bepalen welke genen van de ouders moeten worden overgeërfd.
Om \gls{neuron} genen te kruisen,
worden de innovatienummers gebruikt om te controleren op \emph{gekoppelde}, \emph{ontlede}, en \emph{uitbundige} genen.

Het traditionele \gls{neat} artikel beschrijft twee primaire structuur\glspl{mutation}: nieuwe verbindingsgenen en nieuwe neurongenen.
Daarnaast kunnen andere \glspl{mutation} worden aangebracht.

\section{Probleemstelling}

\Gls{drl} biedt een krachtig raamwerk om een grote verscheidenheid aan taken op te lossen.
Met de toevoeging van \gls{dl} tot \glspl{ann} wordt de verscheidenheid aan beschikbare \glspl{environment}
waarop algoritmen kunnen worden toegepast, sterk uitgebreid.
Standaard implementaties van het \gls{ql} algoritme zijn bijvoorbeeld alleen uitvoerbaar in een beperkt aantal staten- en actie-ruimten,
via een tabellarische methode, of met eenvoudige functies via lineaire functiebenaderingen.
Dit geeft \gls{drl} een ruimer speelveld in termen van toepasbare \glspl{environment}.

Hoewel deze \glspl{ann} complexere omgevingen mogelijk maken,
brengt dit ook met zich mee dat het \gls{ann} moet worden ontworpen,
die voor elke \gls{environment} anders zal zijn.
Het ontwerpen van dit \glspl{ann} kan lastig zijn en voegt onnodige overhead en mogelijke valkuilen toe.
Het is bekend dat \glspl{ann} een enorme hoeveelheid hyperparameters hebben.
Daar komt nog bij dat de gebruikte leerprocessen gewoonlijk de berekening van gradiënten vereisen.
Dit kan resulteren in wat bekend staat als het exploderend en afnemend gradiëntprobleem~\cite{dutta:2018},
waardoor de geleerde parameters ofwel sterk divergeren ofwel convergeren in de tijd.

Een uniform raamwerk dat in elke \gls{environment} zou werken zonder dat er grote parameter aanpassingen nodig zijn,
zou een groot deel van de overhead die door \gls{drl} wordt geïntroduceerd wegnemen.

\section{Voorgestelde methodes}

\subsection{Het NE-DRL raamwerk}

Om ons algoritme op te bouwen, beginnen we met \acrlong{ddql} als basis.
Net als bij het \gls{ddql} algoritme, transformeren we het \acrlong{rl} probleem in een probleem van gesuperviseerd leren.
Het algoritme maakt gebruik van functiebenaderingen om de optimale waardefunctie $q_* \left( s, a \right)$ af te leiden.
Bovendien worden verschillende methoden gebruikt om het leerproces te stabiliseren.
Deze methoden omvatten het gebruik van een secundair netwerk en memory replay.
Bijkomende methodes die kunnen worden toegepast om de leersnelheid en stabiliteit van de \gls{agent} te verbeteren zijn
\emph{Polyak averaging} en \gls{per}.
Deze stabilisatiemethodes --- met eventuele wijzigingen --- zullen worden overgebracht naar ons algoritme,
omdat stabiliseren de grootste hindernis zal zijn om te overwinnen.

Maar waar het traditionele \gls{ddql} algoritme gebruik maakt van \glspl{dnn}
om de optimale niet-lineaire \gls{action}-waarde functie $q_* \left( s, a \right)$ te benaderen,
stellen wij voor om in plaats daarvan \acrlong{ne} toe te passen.
Een diagram van de basisverschillen in de uitvoering is te vinden in Figuur~\ref{fig:drl-vs-ne-drl}.
Het toont diagrammen voor zowel het \gls{drl} raamwerk, als het door ons voorgestelde \gls{nedrl} raamwerk.
\Acrlong{ne} heeft het voordeel dat het automatisch netwerk topologieën kan genereren,
we kunnen deze functie gebruiken om deze last van de gebruiker af te halen.
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{DRL.png}
        \caption{Een eenvoudig schema om te laten zien hoe \acrshort{drl} werkt.}
        \label{subfig:drl}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{NEDRL.png}
        \caption{Een eenvoudig schema om te laten zien hoe \acrshort{nedrl} werkt.}
        \label{subfig:ne-drl}
    \end{subfigure}
    \caption{Enkele diagrammen ter illustratie van de fundamentele verschillen tussen \acrlong{drl} en \acrlong{nedrl}.
        Het belangrijkste verschil ligt in de benadering van de functie.}
    \label{fig:drl-vs-ne-drl}
\end{figure}

Omdat we het traditionele \gls{dnn} model vervangen door een \gls{ne} model,
moeten we er zeker van zijn dat het nieuwe model in staat is te doen wat het oorspronkelijke \gls{dnn} model deed.

In plaats van wiskundig te bewijzen dat het mogelijk is om een functie te benaderen,
hebben we dit uitgetest.
De resultaten kunnen als volgt worden samengevat: het \gls{neat} algoritme is getest en bleek in staat te zijn
om de functie te benaderen, maar alleen onder bepaalde omstandigheden.
Het algoritme bleek in staat te zijn om inputs in batches te verwerken, wat perfect was om toe te passen op de memory replay methode.
Het hanteren van een bewegend doel bleek echter moeilijk te zijn.
Het doelwit moest ofwel zeer onregelmatig bewegen, zodat het algoritme de tijd had om het in te halen,
of veranderen met kleine wijzigingen.

Het bouwen van een functie-benaderingswijze met een \gls{ne} model wordt gedaan met behulp van de \gls{loss-func}.
Onze \gls{fitness} functie zal gewoon de verwachte en werkelijke uitvoerparen itereren
en de cumulatieve fout berekenen met behulp van een voorafbepaalde \gls{loss-func} $\mathcal{L} \left( Y, Y^- \right)$.

De vergelijking voor de \gls{fitness} functie $\mathds{F}$ voor \gls{individual} $\iota$
in het \gls{nedrl} algoritme wordt als volgt toegepast:
\begin{equation}
    \begin{aligned}
        \mathds{F} \left( \iota, Y, Y^- \right) & = - \sum_{y, y^-}^{Y, Y^-} \mathcal{L} \left( y, y^- \right),
    \end{aligned}
    \label{eq:nedrl-fitness}
\end{equation}
waar $Y$ de \gls{action}-waarden van het online model voorstelt,
en $Y^-$ de \gls{action}-waarden van het doelmodel voorstelt.
Zowel $Y$ als $Y^-$ worden bepaald door een uniform verdeelde steekproef uit de terugspeelbuffer $\mathcal{U} \left( D \right)$.

Na elke stap, zal een bepaald aantal \glspl{generation} doorlopen worden,
en het hoogst scorende netwerk wordt aangewezen als het model.
Met alleen deze veranderingen blijkt het algoritme echter enorm onstabiel te zijn.
De \gls{population} kan zich niet snel genoeg aanpassen om het doel bij te houden,
wat grote schommelingen veroorzaakt in de bereikte \gls{fitness}-scores.

\subsection{Het algoritme stabiliseren}

Alleen het \gls{dnn} omwisselen voor een \gls{ne} oplossing was niet voldoende om een goed presterende \gls{agent} te krijgen.
Ons doel is nu om het algoritme te stabiliseren, zodat het in staat is om efficiënt te leren.
We zullen gebruik maken van beproefde methodes die in eerder ontwikkelde algoritmen zoals \gls{dql} en \gls{ddql} worden toegepast,
en stellen wijzigingen in deze methodes voor om het leerproces voor \gls{nedrl} in het bijzonder verder te stabiliseren.

Zoals eerder vermeld, hebben we al enkele standaard technieken toegepast om de stabiliteit te verbeteren,
namelijk een doelmodel, en memory replay.
Door alleen deze methoden te gebruiken zal het algoritme niet genoeg stabiliseren, dus zullen meer technieken nodig zijn.

De techniek die we hier bespreken trekt veel gelijkenissen met \emph{Polyak averaging},
waar de gewichten van het doelnetwerk elke stap geïnterpoleerd worden met het online netwerk.
Aangezien we te maken hebben met verschillende netwerktopologieën, wordt interpolatie tussen verschillende modellen moeilijk.
Daarom hebben we gekozen voor interpolatie tussen de outputwaarden in plaats van de netwerken zelf.
Dit wordt gedaan met de volgende updateregel:
\begin{equation}
    \begin{aligned}
        Q^N_n & = \left( 1 - \frac{n}{N} \right) Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_{i-1} \right) \\
              & + \frac{n}{N} Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_i \right).
    \end{aligned}
    \label{eq:target-interpolation}
\end{equation}
De gewichten van het vorige doelwit worden genoteerd als $\theta^-_{i-1}$,
en de gewichten van het huidige doel worden genoteerd als $\theta^-_i$.
Wanneer het doel elke $N$ stappen wordt geüpdatet, zal de interpolatie lineair zijn over die $N$ stappen,
waarbij $n$ de huidige stap is.

\section{Experimentele resultaten}

\subsection{Agent implementatie}

Voor de implementatie van de \gls{agent} zelf, hebben we gekozen voor een \gls{oop} benadering
voor het implementeren van de \glspl{agent},
met een algemene \gls{rl} \gls{agent} als superklasse, en de verschillende implementaties als subklassen.
Dit resulteert in de \emph{Agent} superklasse die fungeert als een "\gls{rl} pijplijn",
omdat deze alleen het absolute minimum uitvoert dat een \gls{rl} \gls{agent} zou moeten doen,
waarbij de specifieke onderdelen uitwisselbaar zijn.
Bovenop deze pijplijn worden de verschillende andere \glspl{agent} gebouwd.

Het project is geschreven in Python 3.8.10, met uitgebreid gebruik van type hinting.
De belangrijkste bibliotheken die in het project worden gebruikt zijn
\emph{gym}, \emph{numpy}, \emph{tensorflow}, \emph{keras}, en \emph{neat-python}.
De broncode voor het algoritme en de testgevallen zijn te vinden op de volgende URL:
\url{https://gitlab.com/NickVanOsta/ne-drl}

\subsection{De omgeving}

De \gls{environment} die we zullen gebruiken om onze agenten tegen elkaar uit te spelen is de bekende CartPole \gls{environment}.
Specifiek gebruiken we OpenAI Gym's \emph{CartPole-v1} \gls{environment}.
Om de leercapaciteiten van de \gls{agent} te verbeteren wordt een beloningsfunctie
gebaseerd op die van S. Dutta (2018)~\cite{dutta:2018},
de wiskundige notatie ervan is als volgt:
\begin{equation}
    \begin{aligned}
        s                            & = \left\{ x, x_{\theta}, \varphi, \varphi_{\theta} \right\},          \\
        R_{x} \left( s \right)       & = \frac{x_{\theta} - \left| x \right|}{x_{\theta}},                   \\
        R_{\varphi} \left( s \right) & = \frac{\varphi_{\theta} - \left| \varphi \right|}{\varphi_{\theta}}, \\
        R \left( s \right)           & = R_{x} \left( s \right) + R_{\varphi} \left( s \right).
    \end{aligned}
    \label{eq:cartpole-reward}
\end{equation}
Een andere verandering die in de percepties is aangebracht, is de terminatievlag.
De CartPole \gls{environment} markeert een toestand als beëindigd, ook al werd hij in feite na $500$ stappen afgekapt.
Dit probleem werd naar voren gebracht door M. Morales (2020)~\cite{morales:2020},
die hier tegelijkertijd een oplossing voor heeft aangedragen.

\subsection{Hyperparameters}

Een van de belangrijkste aspecten van de evaluatie van \gls{ai} \glspl{agent} zijn de doorgegeven hyperparameters.
In deze sectie zullen we enkele tabellen tonen van de hyperparameters die gebruikt worden bij de evaluatie van \gls{agent}.
Het gaat om de tabellen~\ref{tab:random-params},~\ref{tab:neat-params},~\ref{tab:dql-params}, en~\ref{tab:nedrl-params}.
\begin{table}[h!]
    \centering
    \caption{Een overzicht van de hyperparameters voor de willekeurige \gls{agent}.}
    \label{tab:random-params}

    \begin{tabular}{l c}
        \hline
        Hyperparameter & Waarde \\
        \hline
        $N_L$          & $100$  \\
        $N_T$          & $10$   \\
    \end{tabular}
\end{table}
\begin{table}[h!]
    \centering
    \caption{Een overzicht van de hyperparameters voor de NEAT \gls{agent}.}
    \label{tab:neat-params}

    \begin{tabular}{l c}
        \hline
        Hyperparameter               & Waarde \\
        \hline
        $\gamma$                     & $0.9$  \\
        $\varepsilon_{\text{max}}$   & $1.00$ \\
        $\varepsilon_{\text{min}}$   & $0.01$ \\
        $\varepsilon_{\text{decay}}$ & $0.9$  \\
        $n_{\text{decay}}$           & $1000$ \\
        $G$                          & $1$    \\
    \end{tabular}
\end{table}
\begin{table}[h!]
    \centering
    \caption{Een overzicht van de hyperparameters voor de \acrshort{dql} \gls{agent}.}
    \label{tab:dql-params}

    \begin{tabular}{l c}
        \hline
        Hyperparameter               & Waarde \\
        \hline
        $\gamma$                     & $0.9$  \\
        $\varepsilon_{\text{max}}$   & $1.00$ \\
        $\varepsilon_{\text{min}}$   & $0.01$ \\
        $\varepsilon_{\text{decay}}$ & $0.9$  \\
        $n_{\text{decay}}$           & $1000$ \\
        $\tau_{\text{update}}$       & $100$  \\
        $B$                          & $64$   \\
        $ERB$                        & $4096$ \\
        $E$                          & $1$    \\
    \end{tabular}
\end{table}
\begin{table}[h!]
    \centering
    \caption{Een overzicht van de hyperparameters voor de \acrshort{nedrl} \gls{agent}.}
    \label{tab:nedrl-params}

    \begin{tabular}{l c}
        \hline
        Hyperparameter               & Waarde \\
        \hline
        $\gamma$                     & $0.9$  \\
        $\varepsilon_{\text{max}}$   & $1.00$ \\
        $\varepsilon_{\text{min}}$   & $0.01$ \\
        $\varepsilon_{\text{decay}}$ & $0.9$  \\
        $n_{\text{decay}}$           & $1000$ \\
        $\tau_{\text{update}}$       & $100$  \\
        $B$                          & $64$   \\
        $ERB$                        & $4096$ \\
        $G$                          & $1$    \\
    \end{tabular}
\end{table}

\subsection{NEAT voor functiebenadering}

Voor we onze experimentele resultaten en evaluaties bespreken,
hebben we een klein intermezzo over de toepasbaarheid van \gls{ne} voor functie-benadering.
Om dit te testen zetten we een klein voorbeeld op met twee eenvoudige niet-lineaire functies:
\begin{equation}
    \begin{aligned}
        f_1 \left( x \right) & = \begin{cases}
            \sin x, & x > 0    \\
            \cos x, & x \leq 0
        \end{cases}, \\
        f_2 \left( x \right) & = \left| \sin x \right|.
    \end{aligned}
    \label{eq:non-lin-func-ex-1}
\end{equation}
Bovendien evalueren wij de prestaties bij gebruik van gebatchte en bewegende inputs.
Dit wordt gedaan door willekeurig te bemonsteren voor de doelwaarden
en door de functie om de paar generaties lichtjes aan te passen met een exponentiële factor, als zodanig:
\begin{equation}
    \begin{aligned}
        g_i \left( x \right) & = f_i \left( x \right)^{1 + \left\lfloor \frac{\delta n}{T} \right\rfloor}.
    \end{aligned}
    \label{eq:non-lin-func-ex-2}
\end{equation}

We zullen $20.000$ \glspl{generation} en kijken of het \gls{neat} algoritme
in staat is om de voornoemde functies te benaderen.
We nemen een steekproef van $64$ batches om het model elke \gls{generation} te adapteren,
het doel wordt elke \gls{generation} bijgewerkt ($T = 1$),
en we werken het doel minimaal bij met een waarde van $delta = 0,001$.
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.39\linewidth}
        \includegraphics[width=\linewidth]{data/ne-func-approx/fitness.png}
        \caption{Fitheidsscore over generaties. ($f_1$ bewegend)}
        \label{subfig:error-fitness-f1-moving}
    \end{subfigure}
    \begin{subfigure}{0.51\linewidth}
        \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy.png}
        \caption{Nauwkeurigheid van functie-benadering. ($f_1$ bewegend)}
        \label{subfig:func-approx-accuracy-f1-moving}
    \end{subfigure}

    % \begin{subfigure}{0.39\linewidth}
    %     \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-static.png}
    %     \caption{Fitheidsscore over generaties. ($f_1$ statisch)}
    %     \label{subfig:error-fitness-f1-static}
    % \end{subfigure}
    % \begin{subfigure}{0.51\linewidth}
    %     \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-static.png}
    %     \caption{Nauwkeurigheid van functie-benadering. ($f_1$ statisch)}
    %     \label{subfig:func-approx-accuracy-f1-static}
    % \end{subfigure}

    \caption{De benaderingsresultaten van \acrshort{neat} voor $f_1 \left( x \right) = \begin{cases} \sin x, & x > 0 \\ \cos x, & x \leq 0 \end{cases}$.}
    \label{fig:ne-func-approx-f1}
\end{figure}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.39\linewidth}
        \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-simple.png}
        \caption{Fitheidsscore over generaties. ($f_2$ bewegend)}
        \label{subfig:error-fitness-f2-moving}
    \end{subfigure}
    \begin{subfigure}{0.51\linewidth}
        \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-simple.png}
        \caption{Nauwkeurigheid van functie-benadering. ($f_2$ bewegend)}
        \label{subfig:func-approx-accuracy-f2-moving}
    \end{subfigure}

    % \begin{subfigure}{0.39\linewidth}
    %     \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-simple-static.png}
    %     \caption{Fitheidsscore over generaties. ($f_2$ statisch)}
    %     \label{subfig:error-fitness-f2-static}
    % \end{subfigure}
    % \begin{subfigure}{0.51\linewidth}
    %     \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-simple-static.png}
    %     \caption{Nauwkeurigheid van functie-benadering. ($f_2$ statisch)}
    %     \label{subfig:func-approx-accuracy-f2-static}
    % \end{subfigure}

    \caption{De benaderingsresultaten van \acrshort{neat} voor $f_2 \left( x \right) = \left| \sin x \right|$.}
    \label{fig:ne-func-approx-f2}
\end{figure}
Hoewel het niet perfect overeenkomt met de verwachte functie,
worden de contouren wel aangegeven en volgt hij de kromming redelijk nauwkeurig.

In het algemeen lijkt \gls{neat} perfect in staat om een redelijke functie- benadering te geven.
Vooral met de batched samples en het bewegende doel lijkt het \gls{neat} algoritme in staat
om functies te benaderen, zolang de doelupdates niet te frequent zijn,
of de doelupdates voldoende klein zijn.
De benaderingen hebben de neiging lineaire segmenten te hebben, maar zijn ook in staat gladde secties te genereren.
Zoals te verwachten was,
is het benaderen van eenvoudiger functies gemakkelijker voor het algoritme dan complexere functies.

\subsection{De NE-DRL agent}

Met de veelbelovende resultaten van de vorige sectie,
zijn we begonnen met het implementeren van onze \gls{nedrl} \gls{agent}.
Met onze \gls{environment} ingesteld, hebben we een paar \glspl{agent} getest,
die de volgende algoritmen implementeerden: \gls{dql}, \gls{neat} en\gls{nedrl}.
De resultaten staan hieronder beschreven:
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/dql-per/DQL-4-L-reward.png}
        \caption{(Training, minimum) gemiddelde \gls{reward}: $432.66$}
        \label{subfig:dql-per-rewards-l-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/dql-per/DQL-2-L-reward.png}
        \caption{(Training, maximum) gemiddelde \gls{reward}: $1128.64$}
        \label{subfig:dql-per-rewards-l-max}
    \end{subfigure}

    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/dql-per/DQL-5-T-reward.png}
        \caption{(Testing, minimum) gemiddelde \gls{reward}: $492.37$}
        \label{subfig:dql-per-rewards-t-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/dql-per/DQL-4-T-reward.png}
        \caption{(Testing, maximum) gemiddelde \gls{reward}: $1941.86$}
        \label{subfig:dql-per-rewards-t-max}
    \end{subfigure}

    \caption{De \acrshort{dql} \gls{agent}'s (met \acrshort{per}) \gls{reward} progressie.}
    \label{fig:dql-per-rewards-minmax}
\end{figure}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/neat/NEAT-3-L-reward.png}
        \caption{(Training, minimum) gemiddelde \gls{reward}: $867.66$}
        \label{subfig:neat-rewards-l-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/neat/NEAT-2-L-reward.png}
        \caption{(Training, maximum) gemiddelde \gls{reward}: $1952.41$}
        \label{subfig:neat-rewards-l-max}
    \end{subfigure}

    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/neat/NEAT-3-T-reward.png}
        \caption{(Testing, minimum) gemiddelde \gls{reward}: $373.26$}
        \label{subfig:neat-rewards-t-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/neat/NEAT-0-T-reward.png}
        \caption{(Testing, maximum) gemiddelde \gls{reward}: $1930.63$}
        \label{subfig:neat-rewards-t-max}
    \end{subfigure}

    \caption{De \acrshort{neat} \gls{agent}'s \gls{reward} progressie.}
    \label{fig:neat-rewards-minmax}
\end{figure}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-7-L-reward.png}
        \caption{(Training, minimum) gemiddelde \gls{reward}: $22.05$}
        \label{subfig:nedrl-per-da-rewards-l-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-5-L-reward.png}
        \caption{(Training, maximum) gemiddelde \gls{reward}: $24.18$}
        \label{subfig:nedrl-per-da-rewards-l-max}
    \end{subfigure}

    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-6-T-reward.png}
        \caption{(Testing, minimum) gemiddelde \gls{reward}: $14.81$}
        \label{subfig:nedrl-per-da-rewards-t-min}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-3-T-reward.png}
        \caption{(Testing, maximum) gemiddelde \gls{reward}: $15.71$}
        \label{subfig:nedrl-per-da-rewards-t-max}
    \end{subfigure}

    \caption{De \acrshort{nedrl} \gls{agent}'s (met \acrshort{per} en \acrshort{da}) \gls{reward} progressie.}
    \label{fig:nedrl-per-da-rewards-minmax}
\end{figure}

\section{Conclusion}

We hebben onze \gls{agent} opgebouwd, op basis van het \acrlong{ddql} algoritme,
en verwisselden het standaard \gls{dnn} model met het best presterende netwerk dat door het \gls{neat} algoritme werd gegenereerd.
Maar \gls{neat} is van nature een vluchtige methode, omdat ze met veel willekeur te maken heeft.
Dit stochastische gedrag is uiteindelijk wat deze algoritmes doet leren en ze vooruit stuwt,
maar dit gaat ten koste van de stabiliteit.
Terwijl \glspl{dnn} een zeer stabiele leermethode hebben door gebruik te maken van gradiënten,
leert \gls{ne} met vallen en opstaan.
Om deze instabiliteit binnen onze \gls{agent} tegen te gaan, hebben we een reeks stabilisatietechnieken toegepast.
Onder deze stabilisatietechnieken waren gevestigde methodes die reeds gebruikt werden om het leerproces van algoritmen zoals \gls{dql} te stabiliseren.
Tot deze gevestigde methoden behoorden \acrlong{per} en \acrlong{da}.
Daarnaast hebben we ons gebaseerd op de Polyak averaging die in \gls{dql}
om een soortgelijke interpolatietechniek te creëren die kon worden gebruikt met onze diversiteit in netwerktopologie.

Om de mogelijkheden van onze voorgestelde combinatorische \gls{agent} aan te tonen en te evalueren,
worden experimenten uitgevoerd op OpenAI's \emph{CartPole-v1} \gls{environment}.
Zelfs met alle inspanningen om de agenten te stabiliseren en de hyperparameters te verfijnen,
lijkt onze voorgestelde methode te onstabiel om effectief een consistente strategie te leren voor de gegeven \gls{environment}.
We hebben echter aangetoond dat het voor sommige gelegenheden en \glspl{environment}
gunstig kan zijn om een zuivere \gls{ne} benadering te verkiezen boven een \gls{drl} benadering.

Hoewel ons oorspronkelijk doel was om dit algoritme te ontwikkelen voor gebruik in combinatie met robotcontrole,
waren we door tijdgebrek en onvoorziene hindernissen,
waren we niet in staat om uit te breiden naar complexere \glspl{environment} dan de \emph{CartPole-v1} \gls{environment},
noch waren we in staat om meer \gls{ne} algoritmen uit te testen.

Tenslotte kunnen we concluderen dat de door ons gevolgde aanpak wellicht niet geschikt is voor het oplossen van \gls{rl} problemen.
We hopen dat dit anderen nuttige inzichten zal geven in andere mogelijke gebruikssituaties of misschien zelfs alternatieve methoden
om de twee algoritmen op een meer voordelige manier te combineren.

\balance
\bibliography{refs}

\end{document}