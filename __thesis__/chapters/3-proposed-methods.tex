\chapter{Proposed methods}
\label{ch:proposed-methods}

In this chapter we'll be going over our contributions and the way we tackled the implementation of the \gls{nedrl} algorithm.
Utilizing the preliminary technology and algorithms discussed in Chapter~\ref{ch:background},
we can build up our proposed algorithm.
Although the algorithm will be conceptualized with abstractions in mind,
the algorithm's implementation will be based on \gls{dql} and \gls{neat}~\cite{stanley:2002}.
Our approach assumes the neuro-evolutionary method to be able to handle non-linear function approximation,
as this is necessary for the algorithm to work as intended.
We'll discuss how we built up the \gls{agent} starting from the standard \gls{dql} algorithm,
and explain some issues regarding stability we should tackle and how we tackled them.
We discuss some stabilization techniques we applied to our \gls{agent}
in order to make sure the \gls{agent}'s learning process runs without any issues.
Among these techniques is a target model interpolation method,
derived from the Polyak averaging interpolation technique used in certain \gls{dql} implementations.

\section{The NE-DRL framework}
\label{sec:ne-drl-framework}

To build up our algorithm, we'll start off with \acrlong{dql} as a base.
As with the \gls{dql} algorithm, we'll be transforming the \acrlong{rl} problem into a Supervised Learning problem.
The algorithm will be using function approximation to derive the optimal \gls{action}-value function $q_* \left( s, a \right)$.
On top of that, several methods are employed to stabilize the learning process.
These methods include the usage of a secondary \emph{target} network and memory replay.
Additional methods that can be implemented to improve the \gls{agent}'s learning speed and stability are
\emph{Polyak averaging} and \emph{\gls{per}}.
These stabilizing methods --- with possible alterations --- will be brought over to our algorithm,
as stabilizing will be the largest hurdle to overcome.

However, where traditional \gls{dql} utilizes \glspl{dnn} to approximate the optimal non-linear \gls{action}-value function
$q_* \left( s, a \right)$, we propose to apply \acrlong{ne} instead.
A diagram of the basic differences in implementation can be found in Figure~\ref{fig:drl-vs-ne-drl}.
It shows diagrams for both the \gls{drl} framework, and our proposed \gls{nedrl} framework.
\Acrlong{ne} comes with the added advantage of being able to generate network topologies automatically,
we can leverage this feature to remove this burden from the user.
\begin{figure}[t!]
    \centering
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{DRL.png}
        \caption{A simple diagram illustrating how \acrshort{drl} works.}
        \label{subfig:drl}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.45\linewidth}
        \includegraphics[width=\linewidth]{NEDRL.png}
        \caption{A simple diagram illustrating how \acrshort{nedrl} works.}
        \label{subfig:ne-drl}
    \end{subfigure}
    \caption[The differences between \acrlong{drl} and \acrlong{nedrl}.]
    {Some diagrams illustrating the fundamental differences between \acrlong{drl} and \acrlong{nedrl}.
        The main difference lies within the function approximation approach.}
    \label{fig:drl-vs-ne-drl}
\end{figure}

\section{Neuro-Evolution as a function approximator}
\label{sec:ne-func-approx}

Since we're exchanging the traditional \gls{dnn} with an \gls{ne} model,
we need to be certain that the new model is capable of handling what the \gls{dnn} model originally performed.
Thus, the questions are:
\emph{Can the \gls{ne} model properly approximate non-linear functions?}
\emph{And, if it can, is it fast and accurate enough?}
Mathematically, this is difficult to ascertain.
\Gls{ne} creates a volatile black-box, which makes it hard to determine what is happening exactly behind the scenes.

Instead of mathematically proving \gls{ne}'s capability of handling the necessary function approximation task,
we tested this out.
The exact tests and results will be discussed in Section~\ref{sec:test-ne-func-approx}.
To summarize the results here, the \gls{neat} algorithm was tested and was found to be able
to appropriately handle the function approximation, but only within certain circumstances.
The algorithm proved to be able to handle batched inputs, which was perfect for applying to the memory replay method.
However, handling a moving target proved to be difficult.
The target would either have to move very infrequently, giving the algorithm time to catch up,
or change with small alterations.

\subsection{Building up the function approximator}
\label{subsec:build-func-approx}

Building a function approximator with an \gls{ne} model is fairly simple.
It draws parallels with function approximation using \glspl{ann},
where the algorithm is pushed in the right direction by telling it how much it deviated from the expected solution.
This is done using the \gls{loss-func}.
To correctly migrate this behaviour to our \gls{ne} model,
we have to decide on an appropriate \gls{fitness} function.
Our \gls{fitness} function will just iterate over the batch of expected and actual output pairs
and calculate the cumulative error using a predetermined \gls{loss-func} $\mathcal{L} \left( Y, Y^- \right)$.

The equation for the \gls{fitness} function $\mathds{F}$ for \gls{individual} $\iota$
applied in the \gls{nedrl} algorithm is applied like this:
\begin{equation}
    \begin{aligned}
        \mathds{F} \left( \iota, Y, Y^- \right) & = - \sum_{y, y^-}^{Y, Y^-} \mathcal{L} \left( y, y^- \right),
    \end{aligned}
    \label{eq:nedrl-fitness}
\end{equation}
where $Y$ represents the \gls{action}-values of the online model,
and $Y^-$ represents the \gls{action}-values of the target model.
Both $Y$ and $Y^-$ are determined by a uniformly distributed sample from the replay buffer $\mathcal{U} \left( D \right)$.

After every step, a certain amount of \glspl{generation} will be run,
and the highest scoring network will be assigned as the model.
However, with only these changes the algorithm proves to be immensely unstable.
All goes well during the first few steps, but as the target becomes more complex,
the algorithm starts to destabilize.
The \gls{population} isn't able to adapt quickly enough to catch up with the moving target,
causing major fluctuations within the achieved \gls{fitness} scores.

\section{Stabilizing the algorithm}
\label{sec:stabilize-nedrl}

The basics of our algorithm are in place, but in complex \glspl{environment} the model quickly destabilizes,
resulting in suboptimal performances.
Merely swapping the \gls{dnn} for a \gls{ne} solution was not sufficient to create a well-performing \gls{agent}.
Our goal now is to stabilize the algorithm to ensure it is capable of learning efficiently.
We will make use of tried and tested methods used within previously established algorithms such as \gls{dql} and \gls{ddql},
and propose alterations to those methods to further stabilize the learning process for \gls{nedrl} specifically.

\subsection{Lent from Deep Q-Learning}
\label{subsec:lent-dql}

As mentioned before, we have already implemented some standard techniques --- lent from the \gls{dql} algorithm --- to improve stability,
namely a target model, and memory replay.
These previously established optimizations already do a fine job of stabilizing the network,
but they will still perform poorly due to the erratic nature of \gls{ne}.
Using only these methods will not stabilize the algorithm enough, thus more techniques will be necessary.

\subsection{Double Deep Reinforcement Learning}
\label{subsec:ddrl}

\Gls{ddrl} draws a lot of parallels with standard \gls{drl},
the only difference being the calculation for the target value,
which is updated to the following equation:
\begin{equation}
    \begin{aligned}
        \max_{a'} Q \left( s', a'; \theta^- \right) & = Q \left( s', \argmax_{a'} Q \left( s', a'; \theta^- \right); \theta^- \right)        \\
                                                    & \approx Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^- \right).
    \end{aligned}
    \label{eq:ddrl}
\end{equation}
The standard update rule tends to overestimate the required \gls{action}-value function.
This is caused by taking the maximum estimated value,
which tend to be off-center from the optimal values.
Since we're always taking the maximum value,
a positive bias is created towards higher values,
even though they might be overshooting the goal.

Equation~\eqref{eq:ddrl} shows an alternate way for selecting the best \gls{action}.
"The $\max$ of a Q-function is the same as the Q-function of the $\argmax$ \gls{action}."~\cite{morales:2020}
We're working with an online model and a target model, so the online model can to used to gather the $\argmax$ \gls{action},
as is shown in the latter equation.
This solution prevents the target \gls{action}-values from overshooting,
which positively benefits the \gls{agent}'s learning process by providing more stability.

\subsection{Smoothening the target through interpolation}
\label{subsec:stabilize-with-interpolation}

The tests conducted in Section~\ref{sec:test-ne-func-approx}
show us that \gls{ne} has difficulties when trying to approximate a function with a moving target.
The results of these tests reveal that we either have to update the target very infrequently,
or make sure the target is updated only slightly.
We could combine these two options by interpolating between the targets.

The technique we'll be discussing here draws a lot of parallels with \emph{Polyak averaging},
where the target network weights are interpolated with the online network every step.
Since we're dealing with diverse network topologies, interpolation between different models becomes difficult.
For this reason, we've opted for interpolating between the output values instead of the networks themselves.

The idea is to have the target be updated every so often, while keeping track of the previous target.
We can then interpolate between the previous target and the current target,
resulting in a smooth transition from one target to the next.
This is done with the following update rule:
\begin{equation}
    \begin{aligned}
        Q^N_n & = \left( 1 - \frac{n}{N} \right) Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_{i-1} \right) + \frac{n}{N} Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_i \right).
    \end{aligned}
    \label{eq:target-interpolation}
\end{equation}
The previous target's weights are noted as $\theta^-_{i-1}$,
and the current target's weights are noted as $\theta^-_i$.
When the target is updated every $N$ steps, the interpolation will be a linear one over those $N$ steps,
with $n$ being the current step.

\section*{Summary}
\label{sec:proposed-methods-summary}

In this chapter we built up the theoretical basis for our proposed \gls{nedrl} framework.
We discussed the algorithms we used to build up the \gls{agent} and how exactly they were combined.
But merely combining \gls{dql} and \gls{neat} in our proposed method,
resulted in an unstable learning process.
To combat this, we discussed some additional techniques of stabilizing \gls{drl} \glspl{agent},
and applied them to our proposed algorithm.
Lastly, we built up a target interpolation method,
specifically for target networks of diverse topology,
based upon an established technique called Polyak averaging.
With all of these stabilization techniques in place we have done all we can to finetune the \gls{nedrl} \gls{agent}.
