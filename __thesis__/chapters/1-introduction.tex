\chapter{Introduction}
\label{ch:introduction}

\glsresetall

\Gls{drl} is a rapidly evolving research track in the field of machine learning,
often indicated as the technology that is expected to revolutionize the field of \gls{ai}.
One of the primary goals of the field of \gls{ai} is to develop fully autonomous \glspl{agent} that learn optimal behaviour
by interacting with their \glspl{environment} and improving over time through trial and error.
A mathematical framework for such experience-driven autonomous learning is known as \gls{rl}.
Although \gls{rl} has had some successes in the past, previous approaches lacked scalability
(due to complexity issues), which was limiting their applicability to relatively
low-dimensional problems that are rarely capturing complex real-life scenarios.
The rise of \gls{dl} provided new tools to overcome these problems resulting in a powerful new framework: \gls{drl}.
\Gls{drl} has already demonstrated remarkable results in various applications,
ranging from playing video games~\cite{verleysen:2019} to indoor navigation and with various \glspl{agent} including robots and soft bots~\cite{verleysen:2019, haarnoja:2018, nguyen:2019}.
For example, control policies for robots can be learned directly from camera inputs in the real world~\cite{verleysen:2019}.

Another research track that has been seeing a rise in interest lately is that of \gls{ne}.
\Gls{ne} is an evolutionary method of generating \glspl{ann}
which has proven itself to be very capable of solving a wide assortment of problems and tasks,
including \gls{rl} problems~\cite{salimans:2017, ha:2020}.
Throughout the years a variety of algorithms has been theorized and published,
the most prominent being the following:
\gls{gnarl} (1994)~\cite{angeline:1994},
\gls{neat} (2002)~\cite{stanley:2002},
\gls{cma-es} (2003)~\cite{hansen:2003},
and \gls{eant} (2005 and 2007)~\cite{kassahun:2005, siebel:2007}.
Though these algorithms may seem dated, they provide robust frameworks that are still applicable years after their publication.
\Gls{ne} has been applied to various \gls{rl} problems throughout the years, some examples being:
a wide variety of Atari games~\cite{salimans:2017};
the popular Nintendo game, Super Mario World~\cite{sethbling:2015};
and robotic multi-legged locomotion~\cite{valsalam:2008}.
In some circumstances \gls{ne} was even proven to outperform
\gls{rl} algorithms~\cite{salimans:2017, such:2017}.

\Gls{drl} may be a powerful tool for solving a variety of \gls{rl} problems,
but it comes with some drawbacks.
The main disadvantage when applying \gls{drl} is determining the network topology for the used \gls{ann}.
The \gls{ann}'s topology, and numerous other hyperparameters present in \gls{dl} solutions,
require a fair amount of finetuning, which is currently left for the user to handle.
Our goal is to lighten the burden on the user and let the \gls{agent} handle the finetuning of the \gls{ann}.
To do this, we'll take advantage of the benefits provided by \gls{ne} to generate the network model.

\section{Problem formulation}
\label{sec:problem-formulation}

\Gls{drl} provides a powerful framework for solving a wide variety of tasks.
With the addition of \gls{dl} through \glspl{ann} the variety of available \glspl{environment}
on which \gls{rl} algorithms can be applied is heavily broadened.
For example, standard \gls{ql} implementations are only viable in limited \glspl{state-space} and \glspl{action-space},
through a tabular method, or with simple \gls{policy} functions, through linear function approximation.
This gives \gls{drl} a more sizeable playing field in terms of applicable \glspl{environment}.

Although these \glspl{ann} allow for more complex \glspl{environment},
it also brings with it the need to design the \gls{ann},
which will differ for every \gls{environment}.
Designing these \glspl{ann} can be troublesome and adds unnecessary overhead and possible pitfalls.
\Glspl{ann} are known to have an immense amount of hyperparameters:
the depth of the network, the width of each \gls{layer},
the \glspl{act-func} for each \gls{layer},
the chosen \gls{loss-func}, etc.
On top of that, the learning processes utilized usually require the calculation of gradients.
This can result in what is known as the exploding and diminishing gradient problem~\cite{dutta:2018},
which makes the learned parameters either strongly diverge or converge over time.

Having a uniform framework that would work in every single \gls{environment} without the need for vast parameter adjustments
would lift a lot of the overhead introduced by \gls{drl}.
Additionally, the elimination of the vanishing and exploding gradient problem might improve the \gls{agent}'s learning capabilities.

\section{Research methodology}
\label{sec:research-methodology}

To achieve the proposed objective, the following \emph{research methodology} will be adopted to approach the
problem.

\begin{itemize}
    \item {
          First off, an alternative \gls{drl} algorithm will be theorized,
          which will employ the proposed \gls{ne} techniques.
          Our proposed framework will be built up with the \gls{dql} algorithm as a base.
          The traditional \gls{ann} that is used to perform the function approximation will be exchanged
          for a network generated by the \gls{agent} themselves using \gls{neat}.
          However, this algorithm will prove to be unstable.
          }
    \item {
          In an attempt to improve the \gls{agent}'s stability,
          we discuss several stabilization techniques,
          most of which are established methods already being used within the standard \gls{drl} algorithms.
          Additionally, we introduce a target interpolation technique based upon the Polyak averaging interpolation method
          used for stabilizing the \gls{dql} \gls{agent}.
          Polyak averaging expects the topologies of the two target networks to be the same,
          this is not the case for our \gls{nedrl} \gls{agent},
          where topologies can vary substantially.
          To this extent, we develop an alternative way of interpolating the targets that is not dependent on the target networks sharing topologies.
          }
    \item {
          Lastly this new algorithm will be compared to traditional \gls{rl} algorithms:
          a random \gls{agent} (as a baseline), the \gls{dql} algorithm, an \gls{agent} implementing pure \gls{neat}, and some variants of the \gls{dql} algorithm.
          The algorithms will be compared for multiple \glspl{environment} from the OpenAI Gym library~\cite{openai_gym}.
          The \gls{environment} that will be compared is the \emph{CartPole-v1} environment in the \emph{classic control} section,
          since this \gls{environment} is relatively easier.
          This will serve as a great benchmark for the neuro-evolutionary \gls{drl} algorithm.
          The \glspl{agent} will be evaluated quantitatively and qualitatively on their performance within the \gls{environment}.
          The gained \glspl{reward} and execution times will be utilized as evaluations and compared against each other.
          }
\end{itemize}

\section{Main contribution}
\label{sec:main-contribution}

\Gls{ne} has proven itself to be a promising option for generating \gls{ann} topologies.
\Gls{ne} utilizes evolutionary techniques to adapt the \gls{ann}.
Various \gls{ne} algorithms exist which apply the evolutionary algorithms on different parts of the \gls{ann} topology,
ranging from altering the weights in a fixed topology to completely adjusting the topology itself.
In order to solve the stated problem, the focus should be laid on the latter category of \gls{ne} implementations.
One such algorithm is \gls{neat}~\cite{stanley:2002}, a method that has well proven its worth since its publishing.

The goal is to replace the traditional \gls{ann} used in \gls{drl} with an \gls{ann} generated through \gls{ne}.
This will eliminate the exploding and diminishing gradient problems because \gls{ne} applies no gradient calculations.
Additionally, it will remove the overhead of the \gls{ann} topology design.
Since \gls{ne} already draws some parallels with \gls{rl}, it will only be a small step to combine the two.
Or, at least, that is what was suspected.
In reality however, the proposed \gls{nedrl} algorithm proved to be rather unstable.
This caused us to go search for stabilization techniques,
in order to stabilize and improve the our \gls{agent}.
To improve stability, we implemented some previously established methods, such as:
\begin{itemize}
    \item \gls{ddql},
    \item \gls{per},
    \item and \gls{da}.
\end{itemize}
Additionally, we introduce a target interpolation technique based upon the Polyak averaging interpolation method
used for stabilizing the \gls{dql} \gls{agent}.
Polyak averaging expects the topologies of the two target networks to be the same,
since this is not the case in our proposed framework,
we developed an alternative way of interpolating the targets that is not dependent on the target networks sharing topologies.

All algorithms --- with the exception of \gls{neat} --- were written from scratch in Python.
We opted for this in order to gain an increased understanding of the underlying concepts.
Additionally, all figures in this thesis that are not data plots were self-made.
This was done to ensure the authenticity of the thesis,
and made it possible to present better explanations for our topics.

Initially, our goal was to design a framework that would possibly surpass the established state-of-the-art methods, such as \gls{dql}.
After having worked on this for countless hours and trying anything within our arsenal,
we could not get the proposed algorithm to perform at least a little bit.
Due to the time limit bound to the writing of this thesis, the scope had to be reduced significantly,
and we eventually accepted the fact that the approach we were taking would not be beneficial for creating the framework we initially had in mind.
Nevertheless, the work put into this research was not in vain, we merely found out that the approach we assumed to be groundbreaking
was not a suitable method for solving \gls{rl} problems.

\section{Thesis outline}
\label{sec:thesis-outline}

The remainder of this thesis is composed as follows:
\begin{itemize}
    \item {
          \emph{Chapter 2 Background and related work} contains the fundamental basics and related work upon which this thesis is built.
          It handles concepts such as \glspl{mdp}, tabular \acrlong{rl}, \acrlong{dl}, \acrlong{drl}, \acrlongpl{ga}, and \acrlong{ne}.
          All these topics are explained fairly in-depth, with the \gls{mdp} section even being slowly built up with self-made examples.
          }
    \item {
          \emph{Chapter 3 Proposed methods} deals with the theoretical build up of the \gls{nedrl} framework.
          The framework is built upon the \gls{dql} and \gls{neat} algorithms,
          but with the goal of being as abstract as possible.
          A \gls{fitness} function for the \gls{ne} model within the network is proposed,
          based upon the \gls{loss-func} used within \gls{dql}.
          Since the built up algorithm proved to be somewhat unstable,
          techniques for stabilizing the algorithm are discussed.
          Furthermore, a new method for target interpolation is discussed.
          }
    \item {
          \emph{Chapter 4 Experimental results and discussion} elaborates on our proposed framework described in Chapter~\ref{ch:proposed-methods},
          by presenting thoroughly conducted experimental results.
          The experiments are conducted for the sole reason of evaluating the proposed framework's performance.
          This is done by evaluating seven different \glspl{agent} within the \emph{CartPole-v1} \gls{environment} provided by OpenAI Gym.
          The \glspl{agent} are evaluated on both total gained \glspl{reward} and execution times.
          }
    \item {
          \emph{Chapter 5 Conclusion and future work} concludes the thesis by summarizing the conducted research and obtained results.
          Secondly, Future research topics and possible improvements are discussed.
          And lastly, an ethical and social reflection is performed with regards to research and obtained results.
          }
\end{itemize}
