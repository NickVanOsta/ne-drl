\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}

\LoadClass[11pt, a4paper, openany]{book}
\RequirePackage[a4paper, includeheadfoot, margin=2.50cm]{geometry}

% indent of the first sentence of a paragraph
\setlength{\parindent}{0cm}
% space between paragraphs
\setlength{\parskip}{1em}

% stretch horizontal space between everything
\renewcommand{\baselinestretch}{1.2}

\RequirePackage[hyphens]{url}
\RequirePackage{graphicx}
\RequirePackage{pdfpages}
\RequirePackage{enumitem}
\RequirePackage{float}
\RequirePackage{caption}
\RequirePackage[toc,page]{appendix}
\RequirePackage{fontspec}
\RequirePackage[T1]{fontenc}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{dsfont}

\RequirePackage[labelformat=simple]{subcaption}
\renewcommand\thesubfigure{(\alph{subfigure})}

% Don't indent table of contents, list of figures, and list of tables
\RequirePackage{tocloft}
\setlength{\cftsecindent}{0pt}    % Remove indent for \section
\setlength{\cftsubsecindent}{0pt} % Remove indent for \subsection
\setlength{\cftfigindent}{0pt}    % remove indentation from figures in lof
\setlength{\cfttabindent}{0pt}    % remove indentation from tables in lot

% UGent style guide
\setmainfont[
        Path=fonts/,
        BoldFont=UGentPannoText-SemiBold.ttf,
        ItalicFont=UGentPannoText-Normal.ttf,
        ItalicFeatures={FakeSlant=0.3},
        BoldItalicFont=UGentPannoText-SemiBold.ttf,
        BoldItalicFeatures={FakeSlant=0.3},
]{UGentPannoText-Normal.ttf}
\urlstyle{same} % Also use the default font for URLs

% If you want left justified text, uncomment the line below.
%\RequirePackage[document]{ragged2e} % Left justify all text

% Style Chapter titles so they have the chapter number in grey.
\RequirePackage{color}
\RequirePackage{xcolor}
\definecolor{chaptergrey}{rgb}{0.5,0.5,0.5}
\RequirePackage[explicit, pagestyles]{titlesec}
\titleformat{\chapter}[display]{\bfseries}{\color{chaptergrey}\fontfamily{pbk}\fontsize{80pt}{100pt}\selectfont\thechapter}{0pt}{\Huge #1}
% \titlespacing*{\chapter}{0pt}{-80pt}{30pt}

% Header showing chapter number and title and footer showing page number
\newpagestyle{fancy}{%
        \sethead{} % left
        {} % center
        {\Large\thechapter~~\chaptertitle} %right
        \setfoot{} % left
        {\thepage} % center
        {} %right
        \setheadrule{0pt}
}

% Header showing chapter title and footer showing page number
\newpagestyle{numberless}{%
        \sethead{} % left
        {} % center
        {\Large\chaptertitle} %right
        \setfoot{} % left
        {\thepage} % center
        {} %right
        \setheadrule{0pt}
}

\RequirePackage[ruled,vlined,linesnumbered]{algorithm2e}
\RequirePackage{minted}                                 % for modern code highlighting
\newenvironment{code}{\captionsetup{type=listing}}{}    % To get multiline code fragments working: https://tex.stackexchange.com/a/53540/72273
\renewcommand{\listoflistings}{                         % Add List of Listings to TOC
  \addcontentsline{toc}{chapter}{\listoflistingscaption}%
  \listof{listing}{\listoflistingscaption}%
}

\RequirePackage{hyperref}
\hypersetup{
        colorlinks=true,
        breaklinks=true,
        linkcolor=black,
        anchorcolor=black,
        citecolor=black,
        filecolor=black,
        urlcolor=darkgray
}

\RequirePackage[acronym, toc]{glossaries}
\setacronymstyle{long-short}

\RequirePackage[numbers]{natbib}       % For bibliography; use numeric citations
\bibliographystyle{IEEEtran}
\RequirePackage[nottoc]{tocbibind}     % Put Bibliography in ToC

% Defines \checkmark to draw a checkmark
\RequirePackage{tikz}
% \newcommand{\checkmark}{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

% Defines \blank to insert a blank page
\newcommand{\blank}{\newpage\thispagestyle{empty}\mbox{}}

% For tables
\RequirePackage{multirow}
\RequirePackage{booktabs}
\RequirePackage{array}
\RequirePackage{ragged2e}  % for '\RaggedRight' macro (allows hyphenation)
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% Custom maths commands
\newcommand{\argmax}{
        \mathop{\mathrm{argmax}}\limits
}
\newcommand{\argmin}[1]{
        \mathop{\mathrm{argmin}}\limits
}
