\documentclass[aspectratio=169]{beamer}

\newcommand{\argmax}{
        \mathop{\mathrm{argmax}}\limits
}
\newcommand{\argmin}[1]{
        \mathop{\mathrm{argmin}}\limits
}

% Metadata of the presentation
\title{Neuro-Evolutionary Deep Reinforcement Learning for Robotic Control}
\date[08-09-2022]{Final Defense --- September 8th, 2022}
% \author[PROMOTOR]{Aleksandra Pizurica --- Promotor}
% \author[SUPERVISOR]{Srđan Lazendić --- Supervisor}
\author[NVO]{\textbf{Nick Van Osta} --- \emph{MSc in Information Engineering Technology} \\
\textbf{Aleksandra Pi\v zurica} --- \emph{Promotor} \\
\textbf{Sr{\dj}an Lazendi{\'c}} --- \emph{Supervisor}}
\institute[UGent]{Ghent University\\ ~~\\ Dept. of Electronics and Information Systems (ELIS) \\ Clifford Research Group\\ \&\\  Dept. of Telecommunications and Information Processing (TELIN)\\ Group for Artificial Intelligence and Sparse Modelling (GAIM)}

\usetheme[language=en,faculty=ea,usecolors]{ugent}
\setbeamercolor{block title alerted}{bg=yellow!75, fg=darkgray}
\setbeamercolor{block body alerted}{bg=yellow!25}

% Have this if you'd like section slides
\AtBeginSection[]
{
  \begin{frame}[noframenumbering, plain]
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/shaded,subsectionstyle=show/show/hide,subsubsectionstyle=hide]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}[noframenumbering, plain]
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/shaded,subsectionstyle=show/shaded/hide,subsubsectionstyle=show/hide]
  \end{frame}
}

% packages

\usepackage[T1]{fontenc}
\usepackage{xcolor}
\usepackage{animate}

% \usepackage[numbers]{natbib}       % For bibliography; use numeric citations
% \bibliographystyle{IEEEtran}
\usepackage[backend=bibtex,style=ieee]{biblatex}
\bibliography{refs-minimal}

\usepackage[acronym, toc]{glossaries}

\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{dsfont}
\usepackage{multirow}

\usepackage[ruled,vlined,linesnumbered]{algorithm2e}

\input{chapters/glossary}

\begin{document}

\titleframe

\begin{frame}[noframenumbering, plain]
    \frametitle{Outline}
    \tableofcontents[hideallsubsections]
\end{frame}


\section{Motivation and objective}

% \begin{frame}{Deep Reinforcement Learning}{Benefits \& Drawbacks}
%     \begin{columns}
%         \begin{column}{0.6\linewidth}
%         \end{column}
%         \begin{column}{0.4\linewidth}
%             \includegraphics[width=\linewidth]{DRL.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Deep Reinforcement Learning}{Benefits \& Drawbacks}
%     \begin{columns}
%         \begin{column}{0.6\linewidth}
%             \begin{block}{Benefits}
%                 \begin{itemize}
%                     \item Memory efficiency
%                     \item Ability to handle complex state-spaces
%                           \begin{itemize}
%                               \item High-dimensional state-spaces
%                               \item Continuous state-spaces
%                           \end{itemize}
%                 \end{itemize}
%             \end{block}
%         \end{column}
%         \begin{column}{0.4\linewidth}
%             \includegraphics[width=\linewidth]{DRL.png}
%         \end{column}
%     \end{columns}
% \end{frame}

\begin{frame}{Deep Reinforcement Learning}{Benefits \& Drawbacks}
    \begin{columns}
        \begin{column}{0.6\linewidth}
            \pause[2]
            \begin{block}{Benefits}
                \begin{itemize}
                    \item Memory efficiency
                    \item Ability to handle complex state-spaces
                          \begin{itemize}
                              \item High-dimensional state-spaces
                              \item Continuous state-spaces
                          \end{itemize}
                \end{itemize}
            \end{block}

            \pause[3]
            \begin{block}{Drawbacks}
                \begin{itemize}
                    \item Many hyperparameters $\Rightarrow$ lots of tuning
                    \item Vanishing / exploding gradient problem
                \end{itemize}
            \end{block}
        \end{column}
        \pause[1]
        \begin{column}{0.4\linewidth}
            \includegraphics[width=\linewidth]{DRL.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Objective}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \large
            Minimise \gls{drl}'s drawbacks by using \gls{ne}.

            \vspace{.5cm}
            Alleviate the user's need to design a \gls{dnn}.

            \vspace{.5cm}
            \textbf{Gain insights on the possibility of combining \gls{drl} and \gls{ne}.}
        \end{column}
        \begin{column}{0.5\linewidth}
            \includegraphics[width=\linewidth]{NEDRL.png}
        \end{column}
    \end{columns}
\end{frame}

\section{Background and related work}

\subsection{Deep Reinforcement Learning}

\begin{frame}{Deep Reinforcement Learning}
    \begin{center}
        \includegraphics[width=0.5\linewidth]{DRL.png}
    \end{center}
\end{frame}

\begin{frame}{Markov Decision Processes}
    \begin{center}
        \LARGE{$$
                \begin{aligned}
                    T \left( s, a, s' \right) & = \mathds{P} \left( S_t = s'~|~S_{t-1} = s, A_{t-1} = a \right)      \\
                    R \left( s, a, s' \right) & = \mathds{P} \left( R_t~|~S_{t-1} = s, A_{t-1} = a, S_t = s' \right) \\
                \end{aligned}
            $$}

        \Large{
            $$
                \begin{aligned}
                    A_t & \leftarrow \text{Action took at time step $t$}            \\
                    S_t & \leftarrow \text{State agent resides in at time step $t$} \\
                    T   & \leftarrow \text{Transition function}                     \\
                    R   & \leftarrow \text{Reward function}                         \\
                \end{aligned}
            $$
        }
    \end{center}
\end{frame}

\begin{frame}{Markov Decision Processes}{Bellman Equation}
    \begin{center}
        \Large{$$
                \begin{aligned}
                    v_{\pi} \left(s\right)      & = \sum_{a} \mathds{P} \left( a = \pi \left( s \right) \right) \sum_{s', r} T \left( s, a, s' \right) \left[ r + \gamma v_{\pi} \left( s' \right) \right] \\
                    q_{\pi} \left( s, a \right) & = \sum_{s', r} T \left( s, a, s' \right) \left[ r + \gamma v_{\pi} \left( s' \right) \right]                                                             \\
                \end{aligned}
            $$}
        % \vspace{.5cm}

        \begin{columns}
            \begin{column}{0.5\linewidth}
                \Large{
                    $$
                        \begin{aligned}
                            v_{\pi} \left(s\right)    & \leftarrow \text{State-value function}  \\
                            q_{\pi} \left(s, a\right) & \leftarrow \text{Action-value function} \\
                            \pi                       & \leftarrow \text{Policy}                \\
                            \gamma                    & \leftarrow \text{Discount factor}       \\
                        \end{aligned}
                    $$
                }
            \end{column}
            \begin{column}{0.5\linewidth}
                \Large{
                    $$
                        \begin{aligned}
                            s  & \leftarrow \text{Current state} \\
                            s' & \leftarrow \text{Next state}    \\
                            a  & \leftarrow \text{Action taken}  \\
                            r  & \leftarrow \text{Reward gained} \\
                        \end{aligned}
                    $$
                }
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Markov Decision Processes}{Bellman Equation --- Optimality}
    \begin{center}
        \Large{$$
                \begin{aligned}
                    v_* \left( s \right)    & = \max_a \sum_{s', r} T \left( s, a, s' \right) \left[ r + \gamma v_* \left( s' \right) \right]        \\
                    q_* \left( s, a \right) & = \sum_{s', r} T \left( s, a, s' \right) \left[ r + \gamma \max_{a'} q_* \left( s', a' \right) \right] \\
                \end{aligned}
            $$}

        \begin{columns}
            \begin{column}{0.5\linewidth}
                \Large{
                    $$
                        \begin{aligned}
                            v_{*} \left(s\right)    & \leftarrow \text{Optimal state-value function}  \\
                            q_{*} \left(s, a\right) & \leftarrow \text{Optimal action-value function} \\
                            \pi^*                   & \leftarrow \text{Optimal policy}                \\
                            \gamma                  & \leftarrow \text{Discount factor}               \\
                        \end{aligned}
                    $$
                }
            \end{column}
            \begin{column}{0.5\linewidth}
                \Large{
                    $$
                        \begin{aligned}
                            s  & \leftarrow \text{Current state} \\
                            s' & \leftarrow \text{Next state}    \\
                            a  & \leftarrow \text{Action taken}  \\
                            r  & \leftarrow \text{Reward gained} \\
                        \end{aligned}
                    $$
                }
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

% \begin{frame}{Markov Decision Processes}{Example: Legend of Zelda}
%     \begin{center}
%         \includegraphics[width=0.4\linewidth]{legend-of-zelda-6.png}
%     \end{center}
% \end{frame}

\subsection{Neuro-Evolution}

% \begin{frame}{Neuro-Evolution}{Evolve parameters only}
%     \begin{center}
%         \includegraphics[width=\linewidth]{NE-parameters.png}
%     \end{center}
% \end{frame}

\begin{frame}{Neuro-Evolution}{Evolve parameters and structure}
    \begin{center}
        \includegraphics[width=\linewidth]{NE-structure.png}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution of Augmenting Topologies}{Genotype definition}
    \begin{center}
        \includegraphics[width=0.6\linewidth]{NEAT-genotype.png}
    \end{center}

    \tiny
    \begin{thebibliography}{99}

        \bibitem{M}  K. O. Stanley and R. Miikulainen, \textit{Evolving neural networks through augmenting topologies},
        \newblock Evolutionary computation, vol. 10, no. 2, pp. 99-127, 2002.
    \end{thebibliography}
\end{frame}

\begin{frame}{Neuro-Evolution of Augmenting Topologies}{Example: MarI/O by SethBling}
    \begin{center}
        \animategraphics[autoplay,loop,width=0.6\linewidth]{10}{marIO/marIO-}{0}{290} %290
    \end{center}

    \vspace{0.5cm}
    \tiny
    \begin{thebibliography}{99}

        \bibitem{M}  SethBling, \textit{MarI/O - machine learning for video games},
        \newblock Accessed: 2022-03-15, 2015. [Online].
        \newblock Available: https://www.youtube.com/watch?v=qv6UVOQ0F44
    \end{thebibliography}
\end{frame}

\section{Proposed Methods}

\begin{frame}{Proposed Method}
    \begin{center}
        \includegraphics[width=0.45\linewidth]{NEDRL.png}

        \LARGE{\gls{nedrl}}
    \end{center}
\end{frame}

% \begin{frame}{Proposed Method}
%     \begin{center}
%         \begin{columns}
%             \begin{column}{0.5\linewidth}
%                 What is needed?
%                 \begin{itemize}
%                     \item a \gls{drl} agent to act as a base,
%                     \item a functional \gls{ne} implementation to generate models,
%                     \item and a way to combine the two.
%                 \end{itemize}
%             \end{column}

%             \begin{column}{0.5\linewidth}
%                 \includegraphics[width=\linewidth]{NEDRL.png}
%             \end{column}
%         \end{columns}
%     \end{center}
% \end{frame}

\begin{frame}{Neuro-Evolutionary Deep Reinforcement Learning}{Practical Implementation}
    \begin{center}
        \begin{columns}
            \begin{column}{0.5\linewidth}
                Building blocks:
                \begin{itemize}
                    \item \Gls{ddql} as a base,
                    \item \Gls{neat} as a model generator,
                    \item stabilization techniques.
                \end{itemize}

                \vspace{0.5cm}
                \textbf{Fitness function:}
                $$
                    \begin{aligned}
                        \mathds{F} \left( Y, Y^- \right) & = - \sum_{y, y^-}^{Y, Y^-} \mathcal{L} \left( y, y^- \right) \\
                    \end{aligned}
                $$
            \end{column}

            \begin{column}{0.5\linewidth}
                \includegraphics[width=\linewidth]{NEDRL.png}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolutionary Deep Reinforcement Learning}{stabilization}
    \Large
    \begin{itemize}
        \item \Acrfull{ddql}
        \item \Gls{per}
        \item \Gls{da}
        \item Polyak averaging (altered)
    \end{itemize}
\end{frame}

\begin{frame}{Neuro-Evolutionary Deep Reinforcement Learning}{Target interpolation}
    \LARGE
    Based on Polyak averaging

    \Large
    \vspace{1cm}
    \begin{itemize}
        \item Topologies differ between networks $\Leftarrow$ impossible to interpolate network parameters
        \item Solution: interpolate target model results
    \end{itemize}

    \normalsize
    \begin{center}
        $$
            \begin{aligned}
                Q^N_n & = \left( 1 - \frac{n}{N} \right) Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_{i-1} \right) \\
                      & + \frac{n}{N} Q \left( s', \argmax_{a'} Q \left( s', a'; \theta_i \right); \theta^-_i \right)                        \\
            \end{aligned}
        $$
    \end{center}
\end{frame}

\section{Experimental Results}

\subsection{Neuro-Evolution for function approximation}

\begin{frame}{Neuro-Evolution for function approximation}{Setup}
    \begin{center}
        \begin{columns}[t]
            \begin{column}{0.5\linewidth}
                \Large
                Approximate the following functions:

                \normalsize
                $$
                    \begin{aligned}
                        f_1 \left( x \right) & = \begin{cases}
                            \sin x, & x > 0    \\
                            \cos x, & x \leq 0 \\
                        \end{cases} \\
                        f_2 \left( x \right) & = \left| \sin x \right|
                    \end{aligned}
                $$
            \end{column}

            \begin{column}{0.5\linewidth}
                \Large
                Apply moving target:
                \normalsize
                $$
                    \begin{aligned}
                        g_i \left( x \right) & = f_i \left( x \right)^{1 + \left\lfloor \frac{\delta n}{T} \right\rfloor}, & i \in \left[1, 2\right]
                    \end{aligned}
                $$

                $$
                    \begin{aligned}
                        G      & \leftarrow \text{Amount of generations to run}   \\
                        S      & \leftarrow \text{Batch sample size}              \\
                        \delta & \leftarrow \text{Target move distance}           \\
                        T      & \leftarrow \text{Generations before target move} \\
                        n      & \leftarrow \text{Current generation}             \\
                    \end{aligned}
                $$
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Setup}
    \begin{center}
        \LARGE
        Hyperparameters used:

        \normalsize
        \vspace{1cm}
        \begin{columns}
            \begin{column}{0.5\linewidth}
                $$
                    \begin{aligned}
                        G      & \leftarrow 20,000 \\
                        S      & \leftarrow 64     \\
                        \delta & \leftarrow 0.001  \\
                        T      & \leftarrow 1      \\
                    \end{aligned}
                $$
            \end{column}

            \Leftarrow

            \begin{column}{0.5\linewidth}
                $$
                    \begin{aligned}
                        G      & \leftarrow \text{Amount of generations to run}   \\
                        S      & \leftarrow \text{Batch sample size}              \\
                        \delta & \leftarrow \text{Target move distance}           \\
                        T      & \leftarrow \text{Generations before target move} \\
                    \end{aligned}
                $$
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Results --- $f_1$ (static)}
    \begin{center}
        \begin{columns}[b]
            \begin{column}{0.45\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-static.png}
            \end{column}
            \begin{column}{0.55\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-static.png}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Results --- $f_1$ (moving)}
    \begin{center}
        \begin{columns}[b]
            \begin{column}{0.45\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/fitness.png}
            \end{column}
            \begin{column}{0.55\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy.png}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Results --- $f_2$ (static)}
    \begin{center}
        \begin{columns}[b]
            \begin{column}{0.45\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-simple-static.png}
            \end{column}
            \begin{column}{0.55\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-simple-static.png}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Results --- $f_2$ (moving)}
    \begin{center}
        \begin{columns}[b]
            \begin{column}{0.45\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/fitness-simple.png}
            \end{column}
            \begin{column}{0.55\linewidth}
                \includegraphics[width=\linewidth]{data/ne-func-approx/accuracy-simple.png}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Neuro-Evolution for function approximation}{Summary}
    \begin{center}
        \Large
        \begin{itemize}
            \item Some hiccups, overall fairly accurate
            \item Can handle batched samples
            \item Can handle moving targets*
        \end{itemize}
    \end{center}

    \vspace{1cm}
    * provided either condition is met:
    \begin{itemize}
        \item Frequent target updates \Rightarrow small updates
        \item Large updates \Rightarrow infrequent target updates
    \end{itemize}
\end{frame}

\subsection{NE-DRL performance}

\begin{frame}{Agents and Environment}
    \begin{center}
        \begin{columns}
            \begin{column}{0.5\linewidth}
                \begin{itemize}
                    \item Random Agent (baseline)
                    \item \gls{ddql} --- with \gls{per}
                    \item \gls{neat}
                    \item \gls{nedrl} --- with \gls{per} and \gls{da}
                \end{itemize}
            \end{column}

            \begin{column}{0.5\linewidth}
                \begin{center}
                    \Large
                    \emph{CartPole-v1} from OpenAI Gym

                    \vspace{0.5cm}
                    \animategraphics[autoplay,loop,width=\linewidth]{10}{cartpole/cartpole-}{0}{146} %146
                \end{center}
            \end{column}
        \end{columns}
    \end{center}

    \tiny
    \begin{thebibliography}{99}

        \bibitem{M}  OpenAI, \textit{OpenAI Gym: A toolkit for developing and comparing reinforcement learning algorithms},
        \newblock Accessed: 2022-07-22. [Online].
        \newblock Available: https://www.gymlibrary.dev/
    \end{thebibliography}
\end{frame}

\begin{frame}{Environment}{Altered reward}
    \begin{center}
        \vspace*{0.5cm}
        $$
            \begin{aligned}                                                             \\
                R \left( s \right) & = \frac{x_{\theta} - \left| x \right|}{x_{\theta}} + \frac{\varphi_{\theta} - \left| \varphi \right|}{\varphi_{\theta}} \\
            \end{aligned}
        $$

        \vspace*{0.5cm}
        \small
        \begin{columns}
            \begin{column}{0.5\linewidth}
                $$
                    \begin{aligned}
                        R \left( s \right) & \leftarrow \text{reward for state $s$} \\
                    \end{aligned}
                $$
            \end{column}

            \begin{column}{0.5\linewidth}
                $$
                    \begin{aligned}
                        x                & \leftarrow \text{cart's location}           \\
                        \varphi          & \leftarrow \text{pole's angle}              \\
                        x_{\theta}       & \leftarrow \text{cart's location threshold} \\
                        \varphi_{\theta} & \leftarrow \text{pole's angle threshold}    \\
                    \end{aligned}
                $$
            \end{column}
        \end{columns}

    \end{center}

    \vspace{0.5cm}
    \tiny
    \begin{thebibliography}{99}

        \bibitem{M}  S. Dutta, \textit{Reinforcement Learning with TensorFlow: A beginner's guide to designing self-learning systems with TensorFlow and OpenAI Gym},
        \newblock Packt Publishing Ltd, 2018.
    \end{thebibliography}
\end{frame}

\begin{frame}{Hyperparameters}
    \begin{center}
        \vspace{0.5cm}
        \begin{table}
            \centering
            \begin{tabular}{l c l}
                \hline
                Hyperparameter & Value \\
                \hline
                $N_L$          & $100$ \\
                $N_T$          & $10$  \\
            \end{tabular}
        \end{table}

        \vspace{0.5cm}
        \begin{columns}[t]
            \begin{column}{0.33\linewidth}
                \centering
                \textbf{\gls{ddql}}

                \footnotesize
                \begin{table}
                    \centering
                    \begin{tabular}{l c l}
                        \hline
                        Hyperparameter               & Value  \\
                        \hline
                        $\gamma$                     & $0.9$  \\
                        $\varepsilon_{\text{max}}$   & $1.00$ \\
                        $\varepsilon_{\text{min}}$   & $0.01$ \\
                        $\varepsilon_{\text{decay}}$ & $0.9$  \\
                        $n_{\text{decay}}$           & $1000$ \\
                        $\tau_{\text{update}}$       & $100$  \\
                        $B$                          & $64$   \\
                        $ERB$                        & $4096$ \\
                        $E$                          & $1$    \\
                    \end{tabular}
                \end{table}
            \end{column}

            \begin{column}{0.33\linewidth}
                \centering
                \textbf{\gls{neat}}

                \footnotesize
                \begin{table}[t!]
                    \centering
                    \begin{tabular}{l c l}
                        \hline
                        Hyperparameter               & Value  \\
                        \hline
                        $\gamma$                     & $0.9$  \\
                        $\varepsilon_{\text{max}}$   & $1.00$ \\
                        $\varepsilon_{\text{min}}$   & $0.01$ \\
                        $\varepsilon_{\text{decay}}$ & $0.9$  \\
                        $n_{\text{decay}}$           & $1000$ \\
                        $G$                          & $1$    \\
                    \end{tabular}
                \end{table}
            \end{column}

            \begin{column}{0.33\linewidth}
                \centering
                \textbf{\gls{nedrl}}

                \footnotesize
                \begin{table}
                    \centering
                    \begin{tabular}{l c}
                        \hline
                        Hyperparameter               & Value  \\
                        \hline
                        $\gamma$                     & $0.9$  \\
                        $\varepsilon_{\text{max}}$   & $1.00$ \\
                        $\varepsilon_{\text{min}}$   & $0.01$ \\
                        $\varepsilon_{\text{decay}}$ & $0.9$  \\
                        $n_{\text{decay}}$           & $1000$ \\
                        $\tau_{\text{update}}$       & $100$  \\
                        $B$                          & $64$   \\
                        $ERB$                        & $4096$ \\
                        $G$                          & $1$    \\
                    \end{tabular}
                \end{table}
            \end{column}
        \end{columns}
    \end{center}
\end{frame}

\begin{frame}{Results}{Random Agent --- Worst performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/random/RANDOM-8-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/random/RANDOM-0-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{Random Agent --- Best performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/random/RANDOM-4-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/random/RANDOM-1-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

% \begin{frame}{Results}{DDQL Agent --- Worst performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/dql/DQL-6-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/dql/DQL-4-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{DDQL Agent --- Best performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/dql/DQL-9-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/dql/DQL-8-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

\begin{frame}{Results}{DDQL Agent with PER --- Worst performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/dql-per/DQL-4-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/dql-per/DQL-5-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{DDQL Agent with PER --- Best performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/dql-per/DQL-2-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/dql-per/DQL-4-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{NEAT Agent --- Worst performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/neat/NEAT-3-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/neat/NEAT-3-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{NEAT Agent --- Best performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/neat/NEAT-2-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/neat/NEAT-0-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

% \begin{frame}{Results}{NE-DRL Agent --- Worst performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl/NEAT-DRL-1-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl/NEAT-DRL-5-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{NE-DRL Agent --- Best performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl/NEAT-DRL-4-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl/NEAT-DRL-3-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{NE-DRL Agent with PER --- Worst performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl-per/NEAT-DRL-5-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl-per/NEAT-DRL-0-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{NE-DRL Agent with PER --- Best performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl-per/NEAT-DRL-4-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl-per/NEAT-DRL-2-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{NE-DRL Agent with DA --- Worst performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl-da/NEAT-DRL-3-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl-da/NEAT-DRL-4-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

% \begin{frame}{Results}{NE-DRL Agent with DA --- Best performing}
%     \begin{columns}
%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Training}
%             \includegraphics[width=\linewidth]{data/nedrl-da/NEAT-DRL-2-L-reward.png}
%         \end{column}

%         \begin{column}{0.5\linewidth}
%             \centering
%             \Large
%             \textbf{Testing}
%             \includegraphics[width=\linewidth]{data/nedrl-da/NEAT-DRL-7-T-reward.png}
%         \end{column}
%     \end{columns}
% \end{frame}

\begin{frame}{Results}{NE-DRL Agent with PER \& DA --- Worst performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-7-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-6-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{NE-DRL Agent with PER \& DA --- Best performing}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Training}
            \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-5-L-reward.png}
        \end{column}

        \begin{column}{0.5\linewidth}
            \centering
            \Large
            \textbf{Testing}
            \includegraphics[width=\linewidth]{data/nedrl-per-da/NEAT-DRL-3-T-reward.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Results}{Summary}
    \scriptsize
    \begin{table}
        \centering
        \begin{tabular}{l c r r}
            \hline
            Agent                                                  & Phase    & Exec. Time (s) & Avg. \Gls{reward} \\
            \hline
            \hline
            \multirow{2}{8em}{Random}                              & Training & $3.73$         & $22.66$           \\
                                                                   & Testing  & $0.37$         & $22.43$           \\
            \hline
            \multirow{2}{8em}{\gls{ddql}}                          & Training & $8569.36$      & $955.89$          \\
                                                                   & Testing  & $287.20$       & $1516.42$         \\
            \hline
            \multirow{2}{8em}{\gls{ddql} + \gls{per}}              & Training & $7457.39$      & $815.50$          \\
                                                                   & Testing  & $288.01$       & $1479.70$         \\
            \hline
            \multirow{2}{8em}{\gls{neat}}                          & Training & $2511.71$      & $1500.10$         \\
                                                                   & Testing  & $12.26$        & $1373.83$         \\
            \hline
            \multirow{2}{8em}{\gls{nedrl}}                         & Training & $162.32$       & $23.11$           \\
                                                                   & Testing  & $0.29$         & $23.97$           \\
            \hline
            \multirow{2}{8em}{\gls{nedrl} + \gls{per}}             & Training & $167.93$       & $22.99$           \\
                                                                   & Testing  & $0.19$         & $15.70$           \\
            \hline
            \multirow{2}{8em}{\gls{nedrl} + \gls{da}}              & Training & $194.19$       & $22.71$           \\
                                                                   & Testing  & $0.19$         & $15.60$           \\
            \hline
            \multirow{2}{10em}{\gls{nedrl} + \gls{per} + \gls{da}} & Training & $204.66$       & $22.94$           \\
                                                                   & Testing  & $0.19$         & $15.42$           \\
        \end{tabular}
    \end{table}

    \begin{columns}
    \end{columns}
\end{frame}

\section{Main contributions and Conclusion}

\begin{frame}{Main contributions}
    \Large
    \centering
    \begin{itemize}
        \item Theoretical framework for combining \gls{drl} and \gls{ne}
        \item Interpolation between target networks with different topologies
        \item Insights on the combination of \gls{drl} and \gls{ne}
    \end{itemize}
\end{frame}

\begin{frame}{Conclusion}
    \Large
    \centering
    \begin{itemize}
        \item \Gls{ddql} best option for performance \& accuracy
        \item \Gls{neat} strong alternative when time is key
        \item \Gls{nedrl} might not be a viable option
    \end{itemize}
\end{frame}

\begin{frame}{Outlook}
    \Large
    \centering
    \begin{itemize}
        \item Test out different fitness function definitions
        \item Change target network selection
        \item Try out policy-based or actor-critic algorithms as a \gls{drl} base
        \item Try out more sophisticated \gls{ne} algorithms
    \end{itemize}
\end{frame}

\begin{frame}{References}
    \renewcommand*{\bibfont}{\small}

    \nocite{*}
    \printbibliography
\end{frame}

\end{document}
